# Call this lab rivercrossing.py

import numpy as np
import scipy as sp
import scipy.optimize as opt
import scipy.integrate as it
from scipy.misc import derivative
import matplotlib.pyplot as plt


def cheb(N): 
    x = np.cos((np.pi/N)*np.linspace(N,0,N+1)) 
    x.shape = (N+1,1) 
    lin = np.linspace(0,N,N+1) 
    lin.shape = (N+1,1) 
    c = np.ones((N+1,1)) 
    c[0], c[-1] = 2., 2. 
    c = c*(-1.)**lin 
    X = x*np.ones(N+1) 
    dX = X - X.T 
    D = (c*(1./c).T)/(dX + np.eye(N+1)) 
    D = D - np.diag(np.sum(D.T, axis=0))
    x.shape = (N+1,) 
    return D, x

def prob1(y):
    """
    y is a curve (function) defined on [-1,1] that is passed in as an argument
    return T[y(x)]  (the time required )
    """
    def c(x):
        return -0.7*(x-1)*(x+1)
        
    def dev_y(x):
        return derivative(y,x)
        
    def integrand(x):
        return (1-c(x)**2)**-0.5*(1+x*(dev_y(x)*(1-c(x)**2)**-0.5)**2)**0.5-x*c(x)*dev_y(x)/(1-c(x)**2)
    
    return it.quadrature(integrand,-1,1)[0]
    


def prob2(y):
    """ 
    y is a curve (function) defined on [-1,1] that is passed in as an argument
    Find your upper and lower bounds on T[y].
    return the tuple (lower,upper)
    """
    lower = 2*1 # this is because the sec(theta) has a minimum value of 1
    '''
    def y(x):
        return 2.5+x*2.5
    '''
    upper = prob1(y)
    
    return (lower, upper)
    


def prob3(graph = True):
    """ 
    Find and plot your solution.

    """
    
    D,X = cheb(500)
    
    def y(x):
        return 2.5+x*2.5
    
    def c(x):
        return -0.7*(x-1)*(x+1)
    
    def alpha(x):
        return (1-c(x)**2)**-0.5
    
    def dev_y(x):
        return derivative(y,x)
        
    def Lyprime(x):
        return D.dot(alpha(x)**3*D.dot(y(x))*(1+(alpha(x)*D.dot(y(x)))**2)**-0.5-alpha(x)**2*c(x))
        
    def DLyprime(z):
        return D.dot(alpha(X)**3*D.dot(z)*(1+(alpha(X)*D.dot(z))**2)**-0.5-alpha(X)**2*c(X))

    yx = y(X)

    
    solution = opt.fsolve(DLyprime,yx)
    if graph == True:
        plt.plot(X,y(X))
        plt.plot(X,solution)
        plt.show()
    
    return solution,X
     

def prob4():
    """ 
    Produce the plot asked for in the lab. 
    """
    solution,x = prob3(graph = False)
    X =(x[:-1]+x[1:])/2
    slope = (solution[1:]-solution[:-1])/(x[1:]-x[:-1])#np.diff(solution)
    '''
    plt.plot(X,slope)
    plt.show()
    '''
    
    def c(x):
        return -0.7*(x-1)*(x+1)
    
    def func(z):
        return c(X)*np.cos(z)**-1+np.tan(z)-slope
        
    angles = opt.fsolve(func,np.arctan(5/2)*np.ones(len(X)))
    plt.plot(X,angles)
    '''
    plt.plot(X,np.ones(len(X))*np.pi/6,ls= '--',c='k',label = 'pi/6')
    plt.plot(X,np.ones(len(X))*np.pi/3,ls= '--',c='k',label = 'pi/3')
    plt.plot(X,np.ones(len(X))*np.pi/2,ls= '--',c='k',label = 'pi/2')
    plt.legend()
    '''
    plt.show()
    
        

