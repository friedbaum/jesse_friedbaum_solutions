from __future__ import division
import numpy as np
#import numexpr as ne
from matplotlib import pyplot as plt
from matplotlib import cm

from scipy.misc import imread, imsave
from numpy.random import random_integers, uniform, randn


# Call your solutions file anisotropic_diffusion.py

# Anisotropic diffusion is a method commonly used to remove static from images. 
# For these problems, you will need to use your own images. Since we will not have much 
# time to fine-tune the algorithm, I recommend using smaller images. 
# 
# For each problem below, I will need you to put 1) the original image, and 2) the 'noisy'
# image in your git repository. Do not put the 'denoised' image in the repository; instead 
# your code should denoise and show the image. 

# The code below can be used to add noise to your images. 


def add_noise(imagename,changed_pixels=10000):
	# Adds noise to a black and white image
	# Read the image file imagename.
	# Multiply by 1. / 255 to change the values so that they are floating point
	# numbers ranging from 0 to 1.
	IM = imread(imagename, flatten=True) * (1. / 255)
	IM_x, IM_y = IM.shape
	
	for lost in xrange(changed_pixels):
		x_,y_ = random_integers(1,IM_x-2), random_integers(1,IM_y-2)
		val =  .25*randn() + .5
		IM[x_,y_] = max( min(val+ IM[x_,y_],1.), 0.)
	imsave(name=("noised_"+imagename),arr=IM)

def add_noise_color(imagename,changed_pixels=10000):
	# Adds noise to a black and white image
	# Read the image file imagename.
	# Multiply by 1. / 255 to change the values so that they are floating point
	# numbers ranging from 0 to 1.
	IM = imread(imagename) * (1. / 255)
	IM_x, IM_y, IM_z = IM.shape
	
	for lost in xrange(changed_pixels):
		x_,y_,z_ = random_integers(1,IM_x-2), random_integers(1,IM_y-2), random_integers(1,IM_z-2)
		val =  .25*randn() + .5
		IM[x_,y_,z_] = max( min(val+ IM[x_,y_,z_],1.), 0.)
	imsave(name=("noised_"+imagename),arr=IM)

def anisdiff_bw_noBCs(U, N, lambda_, g):
    """ Run the Anisotropic Diffusion differencing scheme
    on the array A of grayscale values for an image.
    Perform N iterations, use the function g
    to limit diffusion across boundaries in the image.
    Operate on A inplace. """
    for i in xrange(N):
        U[1:-1,1:-1] += lambda_ * \
        (g(U[:-2,1:-1] - U[1:-1,1:-1]) *
        (U[:-2,1:-1] - U[1:-1,1:-1]) +
        g(U[2:,1:-1] - U[1:-1,1:-1]) *
        (U[2:,1:-1] - U[1:-1,1:-1]) +
        g(U[1:-1,:-2] - U[1:-1,1:-1]) *
        (U[1:-1,:-2] - U[1:-1,1:-1]) +
        g(U[1:-1,2:] - U[1:-1,1:-1]) *
        (U[1:-1,2:] - U[1:-1,1:-1]))
        
def anisdiff_bw_withBCs(U, N, lambda_, g):
    """ Run the Anisotropic Diffusion differencing scheme
    on the array U of grayscale values for an image.
    Perform N iterations, use the function g
    to limit diffusion across boundaries in the image.
    Operate on U inplace. """
    difs = np.empty_like(U)
    for i in xrange(N):
        difs[:-1] = g(U[1:] - U[:-1]) * (U[1:] - U[:-1])
        difs[-1] = 0
        difs[1:] += g(U[:-1] - U[1:]) * (U[:-1] - U[1:])
        difs[:,:-1] += g(U[:,1:] - U[:,:-1]) * (U[:,1:] - U[:,:-1])
        difs[:,1:] += g(U[:,:-1] - U[:,1:]) * (U[:,:-1] - U[:,1:])
        difs *= lambda_
        U += difs

def anisdiff_color_withBCs(A, N, lambda_, sigma):
    """ Run the Anisotropic Diffusion differencing scheme
    on the array A of grayscale values for an image.
    Perform N iterations, use the function g = e^{-x^2/sigma^2}
    to limit diffusion across boundaries in the image.
    Operate on A inplace. """
    sinv = -1. / sigma**2
    difs = np.empty_like(A)
    for i in xrange(N):
        difs[:-1] = np.exp(sinv * np.square(A[1:] - A[:-1]).sum(axis=2,keepdims=True)) * (A[1:] - A[:-1])
        difs[-1] = 0
        difs[1:] += np.exp(sinv * np.square(A[:-1] - A[1:]).sum(axis=2,keepdims=True)) * (A[:-1] - A[1:])
        difs[:,:-1] += np.exp(sinv * np.square(A[:,1:] - A[:,:-1]).sum(axis=2, keepdims=True)) * (A[:,1:] - A[:,:-1])
        difs[:,1:] += np.exp(sinv * np.square(A[:,:-1] - A[:,1:]).sum(axis=2, keepdims=True)) * (A[:,:-1] - A[:,1:])
        difs *= lambda_
        A += difs

def prob1():
    # Denoise your (black and white) image using 1) the code given in the lab, and 2) using your code that 
    # uses numexpr to speed up the implementation. 
    # Show the noisy image, and the denoised images. Be sure to put titles on the images so I know what I am looking at.
    # return t_original, t_optimized
    # where t_original is the time spent using the numpy code given in the lab, and t_optimized 
    # is the time spent using your code. 
    add_noise('bat.jpg')
    U = imread('noised_bat.jpg', flatten = True)*(1/255)
    plt.title('Noisey')
    plt.imshow(U,cmap = cm.gray)
    plt.show()
    sigma = 0.7
    lmbd = 0.2
    N = 5
    def g(x):
        return np.exp(-(x/sigma)**2)
    anisdiff_bw_withBCs(U,N,lmbd,g)
    plt.imshow(U,cmap = cm.gray)
    plt.title('Cleaned with lambda = '+str(lmbd)+', sigma = '+str(sigma)+', iterations = '+str(N))
	

def prob2():
    # Denoise your (color) image. Show the noisy image and the denoised image. 
    # return T
    # where T is the time spent denoising the image
    add_noise_color('bat.jpg')
    U = imread('noised_bat.jpg')*(1/255)
    plt.title('Noisey')
    plt.imshow(U)
    plt.show()
    sigma = 0.7
    lmbd = 0.2
    N = 5
    def g(x):
        return np.exp(-(x/sigma)**2)
    anisdiff_color_withBCs(U,N,lmbd,sigma)
    plt.imshow(U)
    plt.title('Cleaned with lambda = '+str(lmbd)+', sigma = '+str(sigma)+', iterations = '+str(N))


def prob3():
    # Denoise your (color) image, using the optimized code. Show the noisy image and the 
    # denoised image.
    # return T
    # where T is the time spent denoising the image
    print 'As this was an optional problem, I elected not to do it.'

