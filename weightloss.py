import numpy as np
from scipy.integrate import ode
import scipy.integrate as sp
from math import log
from matplotlib import pyplot as plt



def prob1():
    """ Using the RK4 method, approximate the solution curve for a 
	single-stage weightloss intervention. 
    Plot and show your solution. 
	"""
    rho_F = 9400.
    rho_L = 1800.
    gamma_F = 3.2
    gamma_L = 22.
    eta_F = 180.
    eta_L = 230.
    C=10.4 #Forbes constant
    beta_AT = 0.14 #Adaptive Thermogenesis
    beta_TEF = 0.1 #Thermic Effect of Feeding
    K=0
    def forbes(F):
        C1 = C * rho_L / rho_F
        return C1 / (C1 + F)
    def energy_balance(F, L, EI, PAL):
        p=forbes(F)
        a1 = (1. / PAL - beta_AT) * EI - K - gamma_F * F - gamma_L * L
        a2 = (1 - p) * eta_F / rho_F + p * eta_L / rho_L + 1. / PAL
        return a1 / a2
    def weight_odesystem(t, y, EI, PAL):
        F, L = y[0], y[1]
        p, EB = forbes(F), energy_balance(F, L, EI, PAL)
        return np.array([(1 - p) * EB / rho_F , p * EB / rho_L])

    def fat_mass(BW, age, H, sex):
        BMI = BW / H**2.
        if sex =='male':
            return BW * (-103.91 + 37.31 * log(BMI) + 0.14 * age) / 100
        else:
            return BW * (-102.01 + 39.96 * log(BMI) + 0.14 * age) / 100
            
    BW = 160 * 0.45359237
    age = 38
    H = 68 * 0.0254
    
    F = fat_mass(BW, age, H, 'female')
    
    L = BW - F

    t = np.linspace(0,1827,1827)
    y = np.zeros((1827,2))
    
    weight = ode(lambda t,y: weight_odesystem(t,y,2025,1.5))
    weight.set_initial_value([F,L],0)
    weight.set_integrator('dopri5')
    while weight.successful() and weight.t < t[-2]:
        y[weight.t]=weight.integrate(weight.t+1)
    
    plt.plot(t,y[:,0],label='Fat')
    plt.plot(t,y[:,1],label='Lean Mass')
    plt.plot(t,y[:,1]+y[:,0],label='Body Weight')
    plt.legend()
    plt.show()
        
    


def prob2():
    rho_F = 9400.
    rho_L = 1800.
    gamma_F = 3.2
    gamma_L = 22.
    eta_F = 180.
    eta_L = 230.
    C=10.4 #Forbes constant
    beta_AT = 0.14 #Adaptive Thermogenesis
    beta_TEF = 0.1 #Thermic Effect of Feeding
    K=0
    def forbes(F):
        C1 = C * rho_L / rho_F
        return C1 / (C1 + F)
    def energy_balance(F, L, EI, PAL):
        p=forbes(F)
        a1 = (1. / PAL - beta_AT) * EI - K - gamma_F * F - gamma_L * L
        a2 = (1 - p) * eta_F / rho_F + p * eta_L / rho_L + 1. / PAL
        return a1 / a2
    def weight_odesystem(t, y, EI, PAL):
        F, L = y[0], y[1]
        p, EB = forbes(F), energy_balance(F, L, EI, PAL)
        return np.array([(1 - p) * EB / rho_F , p * EB / rho_L])

    def fat_mass(BW, age, H, sex):
        BMI = BW / H**2.
        if sex =='male':
            return BW * (-103.91 + 37.31 * log(BMI) + 0.14 * age) / 100
        else:
            return BW * (-102.01 + 39.96 * log(BMI) + 0.14 * age) / 100
            
    BW = 160 * 0.45359237
    age = 38
    H = 68 * 0.0254
    
    F = fat_mass(BW, age, H, 'female')
    
    L = BW - F
    
    t = np.linspace(0,32*7,32*7)
    y = np.zeros((32*7,2))
    

    weight = ode(lambda t,y: weight_odesystem(t,y,1600,1.7))
    weight.set_initial_value([F,L],0)
    weight.set_integrator('dopri5')
    
    while weight.successful() and weight.t < t[16*7]:
        y[weight.t]=weight.integrate(weight.t+1)
        
    weight2 = ode(lambda t,y: weight_odesystem(t,y,2025,1.5))
    weight2.set_initial_value(y[16*7-1],16*7-1)
    weight2.set_integrator('dopri5')
    
    while weight2.successful() and weight2.t < t[32*7-2]:
        y[weight2.t]=weight2.integrate(weight2.t+1)
        
    plt.plot(t,y[:,0],label='Fat')
    plt.plot(t,y[:,1],label='Lean Mass')
    plt.plot(t,y[:,1]+y[:,0],label='Body Weight')
    plt.legend()
    plt.show()




a, b = 0., 13.

alpha = 1. / 3

dim = 2




def Lotka_Volterra(y, x):
    return np.array([y[0] * (1. - y[1]), alpha * y[1] * (y[0] - 1.)])
    
subintervals = 200



def prob3():
    """ Using the RK4 method, approximate the predator-prey trajectories.
    Plot and show the solutions. 
	"""
    y0 = np.array([1 / 2., 1 / 3.])
     
    Y = sp.odeint(Lotka_Volterra,y0,np.linspace(a,b,subintervals))

    Y1, Y2 = np.meshgrid(np.arange(0, 4.5, .2), np.arange(0, 4.5, .2), sparse=True,copy=False)
    U, V = Lotka_Volterra((Y1, Y2), 0)
    
    Q=plt.quiver(Y1[::3,::3],Y2[::3,::3], U[::3,::3], V[::3,::3],pivot='mid',color='b',units='dots',width=3.)
    
    plt.plot(1, 1,'ok',markersize=8)
    plt.plot(0, 0,'ok',markersize=8)
    
    plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
    plt.plot(Y[::10,0], Y[::10,1],'*b')
    
    y0 = np.array([1/2.,3/4.])
    
    Y = sp.odeint(Lotka_Volterra,y0,np.linspace(a,b,subintervals))

    Y1, Y2 = np.meshgrid(np.arange(0, 4.5, .2), np.arange(0, 4.5, .2), sparse=True,copy=False)
    U, V = Lotka_Volterra((Y1, Y2), 0)
    
    Q=plt.quiver(Y1[::3,::3],Y2[::3,::3], U[::3,::3], V[::3,::3],pivot='mid',color='b',units='dots',width=3.)
    
    plt.plot(1, 1,'ok',markersize=8)
    plt.plot(0, 0,'ok',markersize=8)
    
    plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
    plt.plot(Y[::10,0], Y[::10,1],'*b')
    
    y0 = np.array([1/16.,3/4.])
    
    Y = sp.odeint(Lotka_Volterra,y0,np.linspace(a,b,subintervals))

    Y1, Y2 = np.meshgrid(np.arange(0, 4.5, .2), np.arange(0, 4.5, .2), sparse=True,copy=False)
    U, V = Lotka_Volterra((Y1, Y2), 0)
    
    Q=plt.quiver(Y1[::3,::3],Y2[::3,::3], U[::3,::3], V[::3,::3],pivot='mid',color='b',units='dots',width=3.)
    
    plt.plot(1, 1,'ok',markersize=8)
    plt.plot(0, 0,'ok',markersize=8)
    
    plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
    plt.plot(Y[::10,0], Y[::10,1],'*b')
    
    y0 = np.array([1/40.,3/4.])
    
    Y = sp.odeint(Lotka_Volterra,y0,np.linspace(a,b,subintervals))

    Y1, Y2 = np.meshgrid(np.arange(0, 4.5, .2), np.arange(0, 4.5, .2), sparse=True,copy=False)
    U, V = Lotka_Volterra((Y1, Y2), 0)
    
    Q=plt.quiver(Y1[::3,::3],Y2[::3,::3], U[::3,::3], V[::3,::3],pivot='mid',color='b',units='dots',width=3.)
    
    plt.plot(1, 1,'ok',markersize=8)
    plt.plot(0, 0,'ok',markersize=8)
    
    plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
    plt.plot(Y[::10,0], Y[::10,1],'*b')
    
    
    
    
    plt.axis([-.5, 4.5, -.5, 4.5])
    plt.title("Phase Portrait of the Lotka-Volterra Predator-Prey Model")
    plt.xlabel('Prey',fontsize=15)
    plt.ylabel('Predators',fontsize=15)
    plt.show()


def prob4():
    
    alpha = 1
    beta = 1.1


    def Logistic(y,x):
        return np.array([y[0]*(1-y[0]-y[1]),alpha * y[1] * (y[0]- beta)])
        
    y0 = np.array([1 / 3., 1 / 3.])
     
    Y = sp.odeint(Logistic,y0,np.linspace(a,b,subintervals))

    Y1, Y2 = np.meshgrid(np.arange(0, 4.5, .2), np.arange(0, 4.5, .2), sparse=True,copy=False)
    U, V = Logistic((Y1, Y2), 0)
    
    Q=plt.quiver(Y1[::3,::3],Y2[::3,::3], U[::3,::3], V[::3,::3],pivot='mid',color='b',units='dots',width=3.)
    
    plt.plot(1, 1,'ok',markersize=8)
    plt.plot(0, 0,'ok',markersize=8)
    
    plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
    plt.plot(Y[::10,0], Y[::10,1],'*b')
    
    y0 = np.array([1/2.,3/4.])
    
    Y = sp.odeint(Logistic,y0,np.linspace(a,b,subintervals))

    Y1, Y2 = np.meshgrid(np.arange(0, 4.5, .2), np.arange(0, 4.5, .2), sparse=True,copy=False)
    U, V = Logistic((Y1, Y2), 0)
    
    Q=plt.quiver(Y1[::3,::3],Y2[::3,::3], U[::3,::3], V[::3,::3],pivot='mid',color='b',units='dots',width=3.)
    
    plt.plot(1, 1,'ok',markersize=8)
    plt.plot(0, 0,'ok',markersize=8)
    
    plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
    plt.plot(Y[::10,0], Y[::10,1],'*b')
    
    

#prob3()