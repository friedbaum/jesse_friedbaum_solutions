from __future__ import division
import numpy as np
from scipy.integrate import odeint
from matplotlib import pyplot as plt

def find_t(f,a,b,alpha,beta,t0,t1,maxI):
    sol1 = 0
    
    i=0
    while abs(sol1-beta) > 10**-8 and i<maxI:
    
        sol0 = odeint(f,np.array([alpha,t0]), [a,b],atol=1e-10)[1,0]
        sol1 = odeint(f,np.array([alpha,t1]), [a,b],atol=1e-10)[1,0]
        
        t2 =  t1 - (sol1 - beta)*(t1-t0)/(sol1-sol0)
        t0 = t1
        
        t1 = t2
        i=i+1
    
    if i==maxI:
        print "t not found"
    
    return t2

def solveSecant(f,X,a,b,alpha,beta,t0,t1,maxI):

    t=find_t(f,a,b,alpha,beta,t0,t1,maxI)
    sol = odeint(f,np.array([alpha,t]), X,atol=1e-10)[:,0]
    
    return sol


def prob1():

   
    
    def ode(y,x):
    
        return np.array([y[1], -4*y[0]-9*np.sin(x)])
    
    X=np.linspace(0,np.pi,100)
    Y1=solveSecant(ode,X,0,np.pi,1,1,1,3,40)
    Y2=solveSecant(ode,X,0,np.pi,1,1,0,2,40)
    
    plt.plot(X,Y1,'-k',linewidth=2,color='b')
    plt.plot(X,Y2,'-k',linewidth=2,color='g')
    plt.show()
	

def prob2():
    '''
    def find_t(f,a,b,alpha,beta,t0,maxI):
        sol = np.zeros(4)
        
        i=0
        while abs(sol[0]-beta) > 10**-8 and i<maxI:
        
            sol = odeint(f,np.array([alpha,t0,0,1]), [a,b],atol=1e-10)[1,0]
            
            
            t0 =  t0 - (sol[0] - beta)/(sol[3])

            i=i+1
        
        if i==maxI:
            print "t not found"
        
        return t0
    
    def solveSecant(f,X,a,b,alpha,beta,t0,maxI):
    
        t=find_t(f,a,b,alpha,beta,t0,maxI)
        sol = odeint(f,np.array([alpha,t]), X,atol=1e-10)[:,0]
        print sol
        return sol
    '''
    
    def ode(y,x):
    
        return np.array([y[1], 3 + 2*y[0] / float(x**2)])
    
    
    X=np.linspace(1,np.e,100)
    Y1=solveSecant(ode,X,1,np.e,6,np.e**2+6/np.e,np.e**2+6/np.e,-1,50)
    Y2=solveSecant(ode,X,1,np.e,6,np.e**2+6/np.e,np.e**2+6/np.e,-2,50)
    
    plt.plot(X,Y1,'-k',linewidth=2,color='b')
    plt.plot(X,Y2,'-k',linewidth=2,color='g')
    plt.show()


def prob3(n):
    # Using the shooting method, find and return a numerical approximation
    # of the solution to the cannon problem. 
    # n+1 = number of grid points for an approximation
    # domain = np.linspace(0,195,n+1)
    #y, v, theta = approximation of solution at gridpoints in domain. 
    #y, v, theta should have shape (n+1,)
    g = 9.8067
    mu = 0.0003
    
    def ode (t,x):
        return np.array([np.tan(t[2]),-(g*np.sin(t[2])+mu*t[1]**2)/(t[1]*np.cos(t[2])),-g/t[1]**2])
        
    def find_t2(f,a,b,alpha,beta,t0,t1,maxI):
        sol1 = 0
        
        i=0
        while abs(sol1-beta) > 10**-8 and i<maxI:
            #print 'yeah'
            sol0 = odeint(f,np.array([alpha,45,t0]), [a,b],atol=1e-10)[1,0]
            sol1 = odeint(f,np.array([alpha,45,t1]), [a,b],atol=1e-10)[1,0]
            
            t2 =  t1 - (sol1 - beta)*(t1-t0)/(sol1-sol0)
            t0 = t1
            
            t1 = t2
            i=i+1
        
        if i==maxI:
            print "t not found"
        
        return t2
    def solveSecant2(f,X,a,b,alpha,beta,t0,t1,maxI):

        t=find_t2(f,a,b,alpha,beta,t0,t1,maxI)
        sol = odeint(f,np.array([alpha,45,t]), X,atol=1e-10)
        
        return sol
        
        
    domain=np.linspace(1,195,n+1)
    Y1=solveSecant2(ode,domain,0,195,0,10**-7,np.pi/6,np.pi/3,50)
    #print Y1
    y,v,theta = Y1[:,0],Y1[:,1],Y1[:,2]
    plt.plot(domain,y,'-k',linewidth=2,color='b')
    Y1=solveSecant2(ode,domain,0,195,0,10**-7,np.pi/6,0,50)
    #print Y1
    y,v,theta = Y1[:,0],Y1[:,1],Y1[:,2]
    plt.plot(domain,y,'-k',linewidth=2,color='b')
    
    mu = 0;
    
    Y1=solveSecant2(ode,domain,0,195,0,10**-7,np.pi/4,np.pi/3,50)
    #print Y1
    y,v,theta = Y1[:,0],Y1[:,1],Y1[:,2]
    plt.plot(domain,y,'-k',linewidth=2,color='k')
    Y1=solveSecant2(ode,domain,0,195,0,10**-7,np.pi/6,0,50)
    #print Y1
    y,v,theta = Y1[:,0],Y1[:,1],Y1[:,2]
    plt.plot(domain,y,'-k',linewidth=2,color='k')

    plt.show()
    
    
    return domain, (y,v,theta)




