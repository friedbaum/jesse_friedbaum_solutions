# Name your solution file sir.py
#
import numpy as np 
#from scikits import bvp_solver 
import matplotlib.pyplot as plt
from scipy.integrate import odeint 



def prob1(n):
    # n is the number of subintervals in the interval [0,100]
    # x = np.linspace(0,100,n+1) 
    # S, I, R = the solutions at the grid points in x, in one dimensional ndarrays. 
    x = np.linspace(0,100,n+1)
    def func(sir,t):
        result = np.empty(3)
        result[0] = -.5 * sir[0] * sir [1]
        result[1] = .5 * sir[0] * sir[1] -.25 * sir[1]
        result[2] = 0.25 * sir[1]
        return result
    init_condit = np.array([1-6.25E-7,6.25E-7,0])
    SIR = odeint(func,init_condit,x)
    S = SIR[:,0]
    I = SIR[:,1]
    R = SIR[:,2]
    return x, S, I, R 


def prob2():
    
    # Plot and show the solution: S, I, and R, on the interval [0,50]. 
    # Answer the two questions asked in the lab, and return them in 
    # variables three and seven.
    x = np.linspace(0,50,51)
    sir_0 = np.array([1-5/3000000.,5/3000000.,0])
    def func_3(sir,t):
        result = np.empty(3)
        result[0] = -1 * sir[0] * sir [1]
        result[1] = sir[0] * sir[1] -1 * (1./3.) * sir[1]
        result[2] = (1./3.) * sir[1]
        return result
    def func_7(sir,t):
        result = np.empty(3)
        result[0] = -1 * sir[0] * sir [1]
        result[1] = sir[0] * sir[1] -1 * (1./7.) * sir[1]
        result[2] = (1./7.) * sir[1]
        return result
    sir_3 = odeint(func_3,sir_0,x)
    sir_7 = odeint(func_7,sir_0,x)

    '''
    f, (three_day,seven_day) = plt.subplots(2,sharex = True,sharey = True)
    three_day.plot(x,sir_3[:,0],label = 'susceptible')
    three_day.plot(x,sir_3[:,1],label = 'infectious')
    three_day.plot(x,sir_3[:,2],label = 'recovered')
    three_day.set_title('Three day')
    seven_day.plot(x,sir_7[:,0],label = 'susceptible')
    seven_day.plot(x,sir_7[:,1],label = 'infectious')
    seven_day.plot(x,sir_7[:,2],label = 'recovered')
    seven_day.set_title('Seven day')
    plt.legend()
    plt.show()
    '''

    three = 3000000*(1-np.max(sir_3[:,1]))
    seven = 3000000*(1-np.max(sir_7[:,1]))
    return three, seven 

def prob3():
    # Plot and show your solution.
    x = np.linspace(0,1000,1001)
    sir_0 = np.array([1-5/3000000.,5/3000000.,0])
    def func(sir,t):
        result = np.empty(3)
        result[0] = -(3./10.) * sir[0] * sir [1]
        result[1] = (3./10.) * sir[0] * sir[1] -.25 * sir[1]
        result[2] = 0.25 * sir[1]
        return result
    
    SIR = odeint(func,sir_0,x)
    S = SIR[:,0]
    I = SIR[:,1]
    R = SIR[:,2]
    print S
    print I
    print R
    plt.plot(x,S,label = 'susceptible')
    plt.plot(x,I,label = 'infectious')
    plt.plot(x,R,label = 'recovered')
    plt.legend()
    plt.show()



def prob4():
	# Plot and show your solution. 
	
	# Return the constants C mentioned in the problem. Return it in a 
	# one-dimensional array of length 3. 
	return C 

print prob3()

