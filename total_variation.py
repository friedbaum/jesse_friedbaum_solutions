from __future__ import division
import numpy as np
#import numexpr as ne
from matplotlib import pyplot as plt
from matplotlib import cm

from scipy.misc import imread, imsave
from numpy.random import random_integers, uniform, randn


# Call your solutions file total_variation.py

# Total Variation is a method commonly used to remove static from images. 
# For these problems, you will need to use your own images. Since we will not have much 
# time to fine-tune the algorithm, I recommend using smaller images. 
# 
# For each problem below, I will need you to put 1) the original image, and 2) the 'noisy'
# image in your git repository. Do not put the 'denoised' image in the repository; instead 
# your code should denoise and show the image. 

# The code below can be used to add noise to your images. 


def add_noise(imagename,changed_pixels=10000):
    # Adds noise to a black and white image
    # Read the image file imagename.
    # Multiply by 1. / 255 to change the values so that they are floating point
    # numbers ranging from 0 to 1.
    IM = imread(imagename, flatten=True) * (1. / 255)
    IM_x, IM_y = IM.shape
    
    for lost in xrange(changed_pixels):
        x_,y_ = random_integers(1,IM_x-2), random_integers(1,IM_y-2)
        val =  .25*randn() + .5
        IM[x_,y_] = max( min(val+ IM[x_,y_],1.), 0.)
    imsave(name=("noised_"+imagename),arr=IM)




def prob1(name = 'bat.jpg',del_t = 0.001,lmbd = 40,del_x = 1,del_y = 1):
    add_noise(name)
    # Denoise your (black and white) image.
    original = imread(name, flatten = True)
    noisey = imread('noised_'+name, flatten = True)
    plt.imshow(original, cmap=cm.gray)
    plt.title('original')
    plt.show()
    plt.imshow(noisey, cmap=cm.gray)
    plt.title('noisey')
    plt.show()
    #print noisey.shape
    
    def rhs(u,f):
        u_xx = np.roll(u,-1,axis=0)-2*u+np.roll(u,1,axis=0)/(del_x**2)
        u_yy = np.roll(u,-1,axis=1)-2*u+np.roll(u,1,axis=1)/(del_y**2)
        u_x = (np.roll(u,-1,axis=0) - np.roll(u,1,axis=0))/(2.*del_x)
        u_y = (np.roll(u,-1,axis=1) - np.roll(u,1,axis=1))/(2.*del_y)
        
        u -= del_t*(u-f-lmbd*(u_xx+u_yy))
    
    # Show the noisy image, and the denoised images. 
    for i in xrange(250):
        rhs(noisey,original)
    
    plt.imshow(noisey, cmap=cm.gray)
    plt.title('cleaned')
    plt.show()
	

def prob2(name = 'bat.jpg',del_t = 0.001,lmbd = 1,del_x = 1,del_y = 1,epsilon = 2E-7):
    add_noise(name)
    # Denoise your (black and white) image.
    original = imread(name, flatten = True)
    noisey = (imread('noised_'+name, flatten = True))*(1/255)
    plt.imshow(original, cmap=cm.gray)
    plt.title('original')
    plt.show()
    plt.imshow(noisey, cmap=cm.gray)
    plt.title('noisey')
    plt.show()
    # Show the noisy image, and the denoised images. 	
    def rhs(u,f):
        u_xx = np.roll(u,-1,axis=0)-2*u+np.roll(u,1,axis=0)/(del_x**2)
        u_yy = np.roll(u,-1,axis=1)-2*u+np.roll(u,1,axis=1)/(del_y**2)
        u_x = (np.roll(u,-1,axis=0) - np.roll(u,1,axis=0))/(2.*del_x)
        u_y = (np.roll(u,-1,axis=1) - np.roll(u,1,axis=1))/(2.*del_y)
        u_xy = (np.roll(u_x,-1,axis=1) - np.roll(u_x,1,axis=1))/2
        
        u -= del_t*(lmbd*(u-f)-(u_xx*u_y**2+u_yy*u_x**2-2*u_x*u_y*u_xy)/((epsilon+u_x**2+u_y**2)**1.5))
    
    # Show the noisy image, and the denoised images. 
    for i in xrange(200):
        rhs(noisey,original)
    
    plt.imshow(noisey, cmap=cm.gray)
    plt.title('cleaned')
    plt.show()


#prob2(name = 'balloons.jpg')

