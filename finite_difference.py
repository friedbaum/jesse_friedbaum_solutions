# Call this lab finite_difference.py

import numpy as np
import matplotlib.pyplot as plt
import scipy as sp



def prob1(N,a,b):

    """
    Define u(x) = sin( (x + np.pi)**2 - 1).
    
    return x = np.linspace(a,b,N+1)[1:-1] 
    (the interior points on a grid with N equal subintervals),
    your values approximating .5*u''(x) - u'(x), 
    and the differentiation matrix M = .5*D2 - D1
    
    return x, approximation, M
    """
    def func(x):
        return np.sin((x+np.pi)**2-1)
    h = (b-a)/(N*1.)
    #print h
    x = np.linspace(a,b,N+1)[1:-1]
    D2 = (np.eye(N-1)*-2 + np.eye(N-1,k=1)+np.eye(N-1,k=-1)).astype(float)/h**2.
    D1 = (np.eye(N-1,k=1)-np.eye(N-1,k=-1)).astype(float)/(2.*h)
    u = func(x)
    #print D1
    #print D2
    
    udoubleprime = D2.dot(u)
    udoubleprime[0]+=func(a)/(h**2.)
    udoubleprime[-1]+=func(b)/(h**2.)
    #print udoubleprime
    uprime = D1.dot(u)
    uprime[0]-=func(a)/(2.*h)
    uprime[-1]+=func(b)/(2.*h)
    #print uprime
    return(x,udoubleprime*0.5-uprime,0.5*D2 - D1)
 
	


def prob2(N,epsilon):
    """ Numerically approximate the solution of the bvp
    epsilon*u''(x) - u'(x) = f(x), x in (0,1),
    u(0) = 1, u(1) = 3, 
    on the grid x = np.linspace(a,b,N+1).
    
    Plot your solution. 
    """
    alpha = 1
    beta = 3
    b=1
    a=0
    h = (b-a)/(N*1.)
    x = np.linspace(a,b,N+1)
    D = (np.eye(N-1)*-2*epsilon+np.eye(N-1,k=1)*(epsilon-h*0.5)+np.eye(N-1,k=-1)*(epsilon+h*0.5))/(h**2.)
    b = -np.ones(N-1)
    b[0] -= alpha*(epsilon+h/2.)/h**2.
    b[-1] -= beta*(epsilon-h/2.)/h**2.
    #print D
    #print b
    sol = np.empty(N+1)
    sol[1:-1] = np.linalg.solve(D,b)
    sol[0] = alpha
    sol[-1] = beta

    #print sol    
    
    plt.plot(x,sol)
    plt.show()
    
    return sol
    


def prob3(N,epsilon):
    """ 
    Use a log-log plot to demonstrate 2nd order convergence of the finite
    difference method used in approximating the solution of the problem 2 in 
    the lab. 
    
    Do not return anything. 
    """
    num_approx = 10 # Number of Approximations
    N = 5*np.array([2**j for j in range(num_approx)])
    h, max_error = (1.-0)/N[:-1], np.ones(num_approx-1)
    # Best numerical solution, used to approximate the true solution.
    # bvp returns the grid, and the grid function, approximating the solution
    # with N subintervals of equal length.
    num_sol_best = prob2(N[-1],0.1)
    for j in range(len(N)-1):
        num_sol = prob2(N[j],0.1)
        max_error[j] = np.max(np.abs( num_sol- num_sol_best[::2**(num_approx-j-1)] ))
    plt.loglog(h,max_error,'.-r',label="$E(h)$")
    plt.loglog(h,h**(2.),'-k',label="$h^{\, 2}$")
    plt.xlabel("$h$")
    plt.legend(loc='best')
    plt.show()
    print "The order of the finite difference approximation is about ", ( (np.log( max_error[0]) -    np.log(max_error[-1]) )/( np.log(h[0]) - np.log(h[-1]) ) ), "."



""" For problems 4-6, you will need to write (& use :) a function 
that solves a general second order linear bvp with Dirichlet 
conditions at both endpoints:

c1(x)u''(x) + c2(x)u'(x) + c3(x)u(x) = f(x), x in (a,b)
u(a) = alpha, u(b) = beta. 

An appropriate function signature would be something like 
the following: 

def fd_order2(N,a,b,alpha,beta,c1,c2,c3,f): 
	
	return np.linspace(a,b,N+1), numerical_approximation

"""

def fd_order2(N,a,b,alpha,beta,c1,c2,c3,f):
    h = (b-a)/float(N)
    x = np.linspace(a,b,N+1)
    #D_2 = (np.eye(N-1)*-2*c1(x[1:-1])+np.eye(N-1,k=1)*np.roll(c1(x[1:-1]),-1)+np.eye(N-1,k=-1)*np.roll(c1(x[1:-1]),1))/(h**2.)
    #D_1 = (np.eye(N-1,k=1)*np.roll(c2(x[1:-1]),-1) - np.eye(N-1,k=-1)*np.roll(c2(x[1:-1]),1))/float(2*h)
    #D_0 = np.eye(N-1)*c3(x[1:-1])
    #D = D_0 + D_1 + D_2
    
    middle = np.zeros(N-1)
    middle += (c3(x[1:-1])-2*c1(x[1:-1])/(h**2.))
    upper = np.zeros(N-2)
    upper += (c1(x[1:-2])/(h**2.)+c2(x[1:-2])/float(h*2))
    lower = np.zeros(N-2)
    lower += (c1(x[2:-1])/(h**2.)-c2(x[2:-1])/float(h*2))
    
    D = sp.sparse.diags([upper,middle,lower],[1,0,-1])
    #print D.todense()
    
    B = f(x)[1:-1]
    #print B
    
    #print D[:5,:5]
    #print D_0[:5,:5]
    #print D_1[:5,:5]
    #print D_2[:5,:5]
    #print b[-1]
    #print beta
    #print beta*(c1(b)/float(h**2)-c2(b)/float(2*h))
    B[0] = B[0] - alpha*(c1(x[1])-c2(x[1])*h/2.)/float(h**2)
    B[-1] = B[-1] - beta*(c1(x[-2])/float(h**2)+c2(x[-2])/float(2*h))
    #print D[:5,:5]
    #print B
    sol = np.empty(N+1)
    sol[1:-1] = sp.linalg.solve(D.toarray(),B)
    sol[0] = alpha
    sol[-1] = beta
    return x,sol



def prob4(N,epsilon):
    """ Numerically approximate the solution of the problem 4 in the lab.
    
    return x = np.linspace(a,b,N+1), and the approximation u at x
    
    You can check your answer with the following: when N = 10 and epsilon = .1,
    the approximation should be
    	
    approximation = 
    [ 0.         -0.0626756  -0.07445629 -0.07518171 -0.07269296 -0.06765727
     -0.05673975 -0.02697816  0.06334385  0.32759416  1.        ]
    """
    
    def c1(x):
        return epsilon
    def c2(x):
        return 0
    def c3(x):
        return -4 * (np.pi - x**2)
    X,Y = fd_order2(N,0,np.pi/2,0,1,c1,c2,c3,np.cos)
    #plt.plot(X,Y)
    return X,Y


def prob5(N,epsilon):
    """ Numerically approximate the solution of the problem 5 in the lab.
    	
    return x = np.linspace(a,b,N+1), and the approximation u at x
    	
    You can check your answer with the following: when N = 10 and epsilon = .001,
    the approximation should be
    	
    approximation = 
    [-2.         -0.93289014 -1.2830758   0.29230221 -0.13495904  1.13159506
    2.00336499  0.49595211  0.76087388 -0.66890253  0.        ]
    """
    def c1(x):
        return epsilon
    def c2(x):
        return x
    def c3(x):
        return 0
    def f(x):
        return -np.pi * x * np.sin(np.pi * x) -epsilon*np.pi**2*np.cos(np.pi*x)
        
    X,Y = fd_order2(N,-1.,1.,-2.,0.,c1,c2,c3,f)
    
    return X,Y
	

def prob6(N,epsilon):
    """ Numerically approximate the solution of the problem 6 in the lab.
    	
    return x = np.linspace(a,b,N+1), and the approximation u at x
    	
    You can check your answer with the following: when N = 20 and epsilon = .05,
    the approximation should be 
    	
    approximation = 
    [  0.95238095   1.16526611   1.45658263   1.868823     2.47619048
    3.41543514   4.95238095   7.61904762  12.38095238  19.80952381
    24.76190476  19.80952381  12.38095238   7.61904762   4.95238095
    3.41543514   2.47619048   1.868823     1.45658263   1.16526611
    0.95238095]
    	
    	
    """
    def c1(x):
        return epsilon + x**2
    def c2(x):
        return 4 * x
    def c3(x):
        return 2
    def f(x):
        return 0*x
        
    X,Y = fd_order2(N,-1,1,1/(1+epsilon),1/(1+epsilon),c1,c2,c3,f)
    
    return X,Y


print prob6(20,0.05)[1]
print prob5(10,.001)[1]
print prob4(10,.1)