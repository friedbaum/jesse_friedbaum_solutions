# Call this lab inverse_problems.py

from __future__ import division
import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt




def prob1(n):
    """
    n = number of subintervals for the domain [0,1].
    Plot your approximation for a(x) at the grid points 
    x = np.linspace(0,1,n+1)
    """

    cnot = 3/8    
    
    def f(x):
        out = -np.ones(x.shape)
        m = np.where(x<.5)
        out[m] = -6*x[m]**2. + 3.*x[m] - 1.
        return out

    def u(x):
        return (x+1./4)**2. + 1./4
    
    def integral_of_f(x):
        # out = \int_0^x f(s) ds
        out = np.empty(x.shape)
        mask = x<=0.5
        out[mask] = -2*x[mask]**3+(3/2)*x[mask]**2-x[mask]
        mask = np.invert(mask)
        out[mask] = -2*0.5**3+3/2*0.5**2-0.5-(x[mask]-0.5)
        return out
    
    def derivative_of_u(x):
        # out = u'(x)
        return 2*(x+1./4)
    
    x = np.linspace(0,1,n+1)
    F, u_p = integral_of_f(x), derivative_of_u(x)
    #print F
    #plt.plot(F)
    #plt.plot(u_p)
    #plt.show()

    def sum_of_squares(alpha):
        return np.sum(((cnot-F)/alpha - u_p)**2)

    guess = (1./4)*(3-x)
    sol = minimize(sum_of_squares,guess)

    plt.plot(x,sol.x,'-ob',linewidth=2)
    plt.show()
	


def prob2(n,epsilon):
    """
    n = number of subintervals for the domain [0,1].
    epsilon is some parameter value > 0.6605
    Compute your approximation for a(x) at the grid points 
    x = np.linspace(0,1,n+1)

    return x, a(x)
    """
    cnot = 1    
    
    def f(x):
        out = -np.ones(x.shape)
        
        return out

    def u(x):
        return x+1+epsilon*np.sin(epsilon**-2*x)
    
    def integral_of_f(x):
        # out = \int_0^x f(s) ds
        #out = np.empty(x.shape)
        out = -x
        return out
    
    def derivative_of_u(x):
        # out = u'(x)
        return 1 + np.cos(epsilon**-2*x)/epsilon
    
    x = np.linspace(0,1,n+1)
    F, u_p = integral_of_f(x), derivative_of_u(x)
    #print F
    #plt.plot(F)
    #plt.plot(u_p)
    #plt.show()

    def sum_of_squares(alpha):
        return np.sum(((cnot-F)/alpha - u_p)**2)

    guess = (1./4)*(3-x)
    sol = minimize(sum_of_squares,guess)

    plt.plot(x,sol.x,'-ob',linewidth=2)
    plt.show()
    
    return x,sol.x


#prob2(10,0.8)