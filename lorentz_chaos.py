import numpy as np
from mayavi import mlab
from scipy.integrate import odeint
from matplotlib import pyplot as plt


def prob1(samples = 20):
    # Produce the plot requested in the lab.
    alpha = 10
    beta = 8/3.
    rho = 28
    def func(x,t):
        return np.array([alpha*(x[1]-x[0]),rho*x[0]-x[1]-x[0]*x[2],x[0]*x[1]-beta*x[2]])
    y0 = 15*np.random.rand(3,samples)
    t = np.linspace(0,20,1001)
    for i in xrange (samples):
        sol = (odeint(func,y0[:,i],t).T)
        mlab.plot3d(sol[0],sol[1],sol[2],color=(1-i*0.05,0.25,i*0.05))
    mlab.show()

def prob2(m,T,res,step,delay):
    # Produce the plot requested in the lab.
    # 
    # m = number of trajectories to plot,
    # T = final time value
    # res = resolution for the plot
    # step = stepping number = number of new points to include
    # at each update of the plot
    # delay = time delay to use between iterations
    alpha = 10
    beta = 8/3.
    rho = 28
    sol = np.empty((3*m,res))
    lines = []
    def func(x,t):
        return np.array([alpha*(x[1]-x[0]),rho*x[0]-x[1]-x[0]*x[2],x[0]*x[1]-beta*x[2]])
    y0 = 15 *np.random.rand(3,m)
    t = np.linspace(0,T,res)
    for i in xrange(m):
        sol[3*i:3*i+3,:] = (odeint(func,y0[:,i],t).T)
        lines.append(mlab.plot3d(sol[3*i],sol[3*i+1],sol[3*i+2],color=(1-i*1./m,0.25,i*1./m)))
    @mlab.show
    @mlab.animate(delay=delay)
    def animate():
        for i in xrange(2+step,res,step):
            for j in xrange(m):
                lines[j].mlab_source.reset(x=sol[3*j,:i],y=sol[3*j+1,:i],z=sol[3*j+2,:i])
            yield
    animate()
        

def prob3(T,res,step,delay):
    # Produce the plot requested in the lab.
    # 
    # T, res, step, and delay are defined as in prob2
    alpha = 10
    beta = 8/3.
    rho = 28
    def func(x,t):
        return np.array([alpha*(x[1]-x[0]),rho*x[0]-x[1]-x[0]*x[2],x[0]*x[1]-beta*x[2]])
    y0 = 15 *np.random.rand(3)
    t = np.linspace(0,T,res)
    sol1 = (odeint(func,y0,t,atol=1E-14,rtol=1E-12).T)
    sol2 = (odeint(func,y0,t,atol=1E-15,rtol=1E-13).T)
    lines1 = mlab.plot3d(sol1[0],sol1[1],sol1[2],color=(1,0,0))
    lines2 = mlab.plot3d(sol2[0],sol2[1],sol2[2],color =(0,1,0))
    @mlab.show
    @mlab.animate(delay=delay)
    def animate():
        for i in xrange(2+step,res,step):  
            lines1.mlab_source.reset(x=sol1[0,:i],y=sol1[1,:i],z=sol1[2,:i])
            lines2.mlab_source.reset(x=sol2[0,:i],y=sol2[1,:i],z=sol2[2,:i])
            yield
    animate()

def prob4(T,res,step,delay):
    # Produce the plot requested in the lab.
    # 
    # T, res, step, and delay are defined as in prob2
    #
    # Make sure that the ode solver uses stringent values for 
    # the absolute and relative errors: atol= 1e-15, rtol=1e-13
    alpha = 10
    beta = 8/3.
    rho = 28
    def func(x,t):
        return np.array([alpha*(x[1]-x[0]),rho*x[0]-x[1]-x[0]*x[2],x[0]*x[1]-beta*x[2]])
    y0 = 15 *np.random.rand(3)
    py0 = y0 * (1. + 2.22E-16)
    t = np.linspace(0,T,res)
    sol1 = (odeint(func,y0,t).T)
    sol2 = (odeint(func,py0,t).T)
    lines1 = mlab.plot3d(sol1[0],sol1[1],sol1[2],color=(1,0,0))
    lines2 = mlab.plot3d(sol2[0],sol2[1],sol2[2],color =(0,1,0))
    @mlab.show
    @mlab.animate(delay=delay)
    def animate():
        for i in xrange(2+step,res,step):  
            lines1.mlab_source.reset(x=sol1[0,:i],y=sol1[1,:i],z=sol1[2,:i])
            lines2.mlab_source.reset(x=sol2[0,:i],y=sol2[1,:i],z=sol2[2,:i])
            yield
    animate()


def prob5():
    # Produce the plot requested in the lab.
    # 
    # calculate and return your approximation for the Lyapunov 
    # exponent
    
    #return exponent
    alpha = 10
    beta = 8/3.
    rho = 28
    def func(x,t):
        return np.array([alpha*(x[1]-x[0]),rho*x[0]-x[1]-x[0]*x[2],x[0]*x[1]-beta*x[2]])
    y0 = np.array([8,8,8])
    py0 = y0 * (1. + 2.22E-16)
    t = np.linspace(0,9,1000)
    sol1 = (odeint(func,y0,t,atol=1E-15,rtol=1E-13).T)
    sol2 = (odeint(func,py0,t,atol=1E-15,rtol=1E-13).T)
    error = np.abs(sol1-sol2)
    error = (error[0]**2+error[1]**2+error[2]**2)**0.5
    x = np.polyfit(t,np.log(error),1)
    y = t*x[0] + x[1]
    plt.semilogy(t,np.e**y)
    plt.semilogy(t,error)
    plt.show()
    
    return x[0]
    

print prob5()