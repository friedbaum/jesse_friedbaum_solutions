from __future__ import division
import numpy as np
from matplotlib import pyplot as plt


def initialize_all(y0, t0, t, n):
    """ An initialization routine for the different ODE solving
    methods in the lab. This initializes Y, T, and h. """
    if isinstance(y0, np.ndarray):
        Y = np.empty((n, y0.size)).squeeze()
    else:
        Y = np.empty(n)
    Y[0] = y0
    T = np.linspace(t0, t, n)
    h = float(t - t0) / (n - 1)
    return Y, T, h


def func_1(x,y):
        return y - 2 * x + 4


def euler(f, y0, t0, t, n):
    """ Use the Euler method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    Y,T,h = initialize_all(y0, t0, t, n)
    
    for i in xrange(1,n):
        Y[i] = Y[i-1] + f(T[i-1],Y[i-1]) * h
    
    return Y,T


# optional
def backwards_euler(f, fsolve, y0, t0, t, n):
    """ Use the backward Euler method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    fsolve is a function that solves the equation
    y(x_{i+1}) = y(x_i) + h f(x_{i+1}, y(x_{i+1}))
    for the appropriate value for y(x_{i+1}).
    It should accept three arguments.
    The first should be the value of y(x_i).
    The second should be the distance between values of t.
    The third should be the value of x_{i+1}.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    pass


def midpoint(f, y0, t0, t, n):
    """ Use the midpoint method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    Y,T,h = initialize_all(y0, t0, t, n)
    
    m = h/2

    for i in xrange(0,n-1):
        Y[i+1] = Y[i] + h * f(T[i]+m,Y[i] + m * f(T[i],Y[i]))

    return Y,T
    
def RG4(f, y0, t0, t, n):
    Y,T,h = initialize_all(y0, t0, t, n)
    
    m = h/2
    
    for i in xrange(0,n-1):
        K1 = f(T[i],Y[i])
        K2 = f(T[i] + m, Y[i] + m * K1)
        K3 = f(T[i] + m, Y[i] + m * K2)
        K4 = f(T[i+1],Y[i] + h * K3)
        Y[i+1] = Y[i] + h * (K1+2*K2+2*K3+K4) / 6
        
    
    return Y,T



def prob1():
    
    def solution(x):
        return -2 + 2 * x + 2 * np.e**(x)    
    
    y2,T2 = euler(func_1,0,0,2,10)
    y3,T3 = euler(func_1,0,0,2,5)
    y1,T1 = euler(func_1,0,0,2,20)
    #y4,T4 = midpoint(func_1,0,0,2,20)
    #y5,T5 = RG4(func_1,0,0,2,20)
    
    y = solution(np.linspace(0,2,100))
    
    plt.plot(np.linspace(0,2,100),y, label = "solution")
    plt.plot(T1,y1, label = "h = 0.1")
    plt.plot(T2,y2, label = "h = 0.2")
    plt.plot(T3,y3, label = "h = 0.4")
    #plt.plot(T4,y4, label = "midpoint")
    #plt.plot(T5,y5, label = "Runge-Kutta")
    plt.legend()
    plt.plot()
    
def prob2():
    val = 2*(1+np.e**2)
    ns = np.array([10,20,40,80,160])
    euler_error = np.abs(np.array([euler(func_1,0,0,2,i)[0][-1] for i in ns])-val)/val
    midpoint_error = np.abs(np.array([midpoint(func_1,0,0,2,i)[0][-1] for i in ns])-val)/val
    RG4_error = np.abs(np.array([RG4(func_1,0,0,2,i)[0][-1] for i in ns])-val)/val
    h = np.array([0.2,0.1,0.05,0.025,0.0125])
    plt.loglog(h,euler_error, label = "Euler")
    plt.loglog(h,midpoint_error, label = "Midpoint")
    plt.loglog(h,RG4_error, label = "Runge-Kutta 4")
    plt.legend()
    plt.show()
    
def prob3(n):
    y0 = np.array([2,-1])
    t0=  0
    k = 1
    m = 1
    def func_2(x,y):
       return np.array([y[1],-k*float(y[0]/m)])
       
    y1,T1 = RG4(func_2,y0,t0,20,n)
    #print y1
    #print T1
    m = 3
    y2,T2 = RG4(func_2,y0,t0,20,n)
    
    plt.plot(T1,y1[:,0],label = "m = 1")
    plt.plot(T2,y2[:,0],label = "m = 3")
    plt.legend()
    plt.show()
    
def prob4(n,gamma):
    y0 = np.array([1,-1])
    t0=  0

    def func_2(x,y):
       return np.array([y[1],-y[0]-gamma*y[1]])
       

        
    y1,T1 = RG4(func_2,y0,t0,20,n)
    #plt.plot(T1,y1[:,0])
            
    #plt.legend()
    #plt.show()   
    
    return y1[:,0]


def prob5(n,gamma,omega):
    y0 = np.array([2,-1])
    t0 = 0
    
    def func_3(x,y):
        return np.array([y[1],-(gamma/2)*y[1]-y[0]+np.cos(omega*x)])
        
    y1,T1 = RG4(func_3,y0,t0,40,n)
    #plt.plot(T1,y1[:,0])
        
        
    #plt.legend()
    #plt.show()
    return y1[:,0]
 
    
    
#Problem_5(160,0,1)
    
    
    
    