# Call your solutions file heatflow.py
import numpy as np
from matplotlib import animation, pyplot as plt


def prob1():
    # For the initial boundary value problem given, plot
    # the initial data and the approximation at t = .4 on the same figure.
    v = 0.05
    k = 0.4/10
    h = 1./6
    lmb = v*k/h**2
    A = np.eye(7)-np.eye(7)*2*lmb+np.eye(7,k=1)*lmb+np.eye(7,k=-1)*lmb
    u0 = np.maximum(np.zeros(7),2*(-np.abs(np.array([x-0.5 for x in np.linspace(0,1,7)]))+0.2))
    plt.plot(np.linspace(0,1,7),u0)
    plt.title('U^0')
    plt.show()
    for i in xrange(10):
        u0 = A.dot(u0)
        plt.plot(np.linspace(0,1,7),u0)
        plt.title('U^'+ str(i+1))
        plt.show()
    	

def sine_animation(res=100):
    # Make the x and y data.
    x = np.linspace(-1, 1, res+1)[:-1]
    y = np.sin(np.pi * x)
    # Initialize a matplotlib figure.
    f = plt.figure()
    # Set the x and y axes by constructing an axes object.
    plt.axes(xlim=(-1,1), ylim=(-1,1))
    # Plot an empty line to use in the animation.
    # Notice that we are unpacking a tuple of length 1.
    line, = plt.plot([], [])
    # Define an animation function that will update the line to
    # reflect the desired data for the i'th frame.
    def animate(i):
        # Set the data for updated version of the line.
        line.set_data(x, np.roll(y, i))
        # Notice that this returns a tuple of length 1.
        return line,
    a = animation.FuncAnimation(f, animate, frames=y.size, interval=20)
    # Show the animation.
    plt.show()  
	
	
def prob2():
    # Produce two animations using the given discretizations in space and time. 
    # The second animation should show instability in the numerical solution, due to 
    # the CFL condition being broken. 
    	
    # You may use either matplotlib or mayavi for the animations.
    v = 1
    k = 1./70
    h = 24./140
    lmb = v*k/h**2
    A = np.eye(141)-np.eye(141)*2*lmb+np.eye(141,k=1)*lmb+np.eye(141,k=-1)*lmb
    u = np.zeros((70,141))
    u[0] = np.maximum(np.zeros(141),np.array([1-x**2 for x in np.linspace(-12,12,141)]))
    for i in xrange(1,70):
        u[i,:] =  A.dot(u[i-1])
    
    x = np.linspace(-12, 12, 141)
    
    # Initialize a matplotlib figure.
    f = plt.figure()
    # Set the x and y axes by constructing an axes object.
    plt.axes(xlim=(-12,12), ylim=(-0.1,1.5))
    # Plot an empty line to use in the animation.
    # Notice that we are unpacking a tuple of length 1.
    line, = plt.plot([], [])
    # Define an animation function that will update the line to
    # reflect the desired data for the i'th frame.
    

    def animate(i):
        # Set the data for updated version of the line.
        #locs['u0'] = A.dot(u0)
        #print u0
        line.set_data(x, u[i])
        # Notice that this returns a tuple of length 1.
        return line,
    a = animation.FuncAnimation(f, animate, frames=70, interval=20)
    # Show the animation.
    plt.show()      
    
    v = 1
    k = 1./66
    h = 24./140
    lmb = v*k/h**2
    A = np.eye(141)-np.eye(141)*2*lmb+np.eye(141,k=1)*lmb+np.eye(141,k=-1)*lmb
    u = np.zeros((70,141))
    u[0] = np.maximum(np.zeros(141),np.array([1-y**2 for y in np.linspace(-12,12,141)]))
    for i in xrange(1,66):
        u[i,:] =  A.dot(u[i-1])
    
    x = np.linspace(-12, 12, 141)
    
    # Initialize a matplotlib figure.
    f = plt.figure()
    # Set the x and y axes by constructing an axes object.
    plt.axes(xlim=(-12,12), ylim=(-0.1,1.5))
    # Plot an empty line to use in the animation.
    # Notice that we are unpacking a tuple of length 1.
    line, = plt.plot([], [])
    # Define an animation function that will update the line to
    # reflect the desired data for the i'th frame.
    

    def animate(i):
        # Set the data for updated version of the line.
        #locs['u0'] = A.dot(u0)
        #print u0
        line.set_data(x, u[i])
        # Notice that this returns a tuple of length 1.
        return line,
    a = animation.FuncAnimation(f, animate, frames=66, interval=20)
    # Show the animation.
    plt.show()
     
    	

def prob3():
    # Reproduce the loglog plot in the lab to demonstrate 2nd order convergence of the 
    # Crank-Nicolson method.
    solutions = []
    steps = [20,40,80,160,320,640] 
    for i in steps:
        #print i
        v = 1
        k = 1./i
        h = 24./i
        lmb = v*k/(2*h**2)
        A = np.eye(i+1)-np.eye(i+1)*2*lmb+np.eye(i+1,k=1)*lmb+np.eye(i+1,k=-1)*lmb
        #print A
        B = np.eye(i+1)+np.eye(i+1)*2*lmb-np.eye(i+1,k=1)*lmb-np.eye(i+1,k=-1)*lmb
        B[0,0],B[0,1],B[-1,-1,],B[-1,-2] = 1,0,1,0

        u0 = np.maximum(np.zeros(i+1), np.array([1-y**2 for y in np.linspace(-12,12,i+1)]))
        #print u0
        
        for j in xrange(i):
            u0 = A.dot(u0)
            u0 = np.linalg.solve(B,u0)
        
        solutions.append(u0)
        
    errors = []
    
    for i in xrange(len(steps)):
        errors.append(np.max(np.abs(solutions[-1][::640/steps[i]]-solutions[i])))

    spaces = []
    squarespaces = []
    for i in steps:
        space = 24./i
        spaces.append(space)
        squarespaces.append(space)
    
    plt.loglog(spaces,errors, label = 'Error')

    plt.loglog(spaces,squarespaces, label = 'h')
    plt.legend()
    plt.show()

