# Call your solutions file waves.py
import numpy as np
import scipy as sp
from matplotlib import animation, pyplot as plt

def wave_solver(s,ut,u0,ddu,J,M,tf):
    tstep = 1/float(J)
    dstep = 1/float(M)
    
    tsteps = np.linspace(0,tf,J+1)
    dsteps = np.linspace(0,1,M+1)
    
    
    lmd = s*tstep/dstep
    
    A = np.diag(np.ones(M+1)*2*(1-lmd**2)) + np.diag(np.ones(M)*lmd**2,k=1) + np.diag(np.ones(M)*lmd**2,k=-1)
    
    U = np.empty((J+1,M+1))
    U[0] = u0(dsteps)
    U[1] = U[0] + ut(dsteps) * tstep + s**2*ddu(dsteps)*tstep**2/2.
    
    for i in xrange(2,J+1):
        #print A.shape
        #print U.shape
        U[i] = A.dot(U[i-1])-U[i-2]
        U[i,0],U[i,-1]= 0,0
    
    return U,dsteps


def prob1():
    # For the initial boundary value problem given, plot
    # the true solution and the approximation at t = .5 on the same figure.

    def u0(x):
        return np.sin(2*np.pi*x)
    
    def ut(x):
        return 0
        
    def ddu(x):
        return -4*np.pi**2*np.sin(2*np.pi*x)
        
    approximated = wave_solver(1,ut,u0,ddu,5,5,0.5)
    
    domain = np.linspace(0,1,6)
    
    solution = np.sin(np.pi*2*domain)*np.cos(np.pi)
    
    #print approximated[1]
    #print approximated[1][-1]
    
    plt.plot(domain,solution,label = 'True Solution')
    plt.plot(approximated[1],approximated[0][-1],label = 'Approximiation')
    plt.legend()
    plt.show()      
         

         
	
	
	
def prob2():
    # Produce two animations using the given discretizations in space and time. 
    # The second animation should show instability in the numerical solution, due to 
    # the CFL stability condition being broken. 
	
    # You may use either matplotlib or mayavi for the animations.
    
    m = 20
 
 
    def u0(x):
        return 0.2*np.e**(-m**2*(x-0.5)**2)
    
    def ut(x):
        return -0.4*m**2*(x-0.5)*np.e**(-m**2*(x-0.5)**2)
        
    def ddu(x):
        return (-0.4 * m**2 + 0.8 * m**4 * (0.5-x)**2)*np.e**(-m**2 * (-0.5 + x)**2)
        
    u,domain = wave_solver(1,ut,u0,ddu,220,200,1)
    
    # Initialize a matplotlib figure.
    f = plt.figure()
    # Set the x and y axes by constructing an axes object.
    plt.axes(xlim=(0,1), ylim=(-.5,.5))
    # Plot an empty line to use in the animation.
    # Notice that we are unpacking a tuple of length 1.
    line, = plt.plot([], [])
    # Define an animation function that will update the line to
    # reflect the desired data for the i'th frame.
    

    def animate(i):
        # Set the data for updated version of the line.
        #locs['u0'] = A.dot(u0)
        #print u0
        line.set_data(domain, u[i])
        # Notice that this returns a tuple of length 1.
        return line,
    a = animation.FuncAnimation(f, animate, frames=221, interval=20)
    # Show the animation.
    plt.show()


    u,domain = wave_solver(1,ut,u0,ddu,180,200,1)

    # Initialize a matplotlib figure.
    f = plt.figure()
    # Set the x and y axes by constructing an axes object.
    plt.axes(xlim=(0,1), ylim=(-.5,.5))
    # Plot an empty line to use in the animation.
    # Notice that we are unpacking a tuple of length 1.
    line, = plt.plot([], [])
    # Define an animation function that will update the line to
    # reflect the desired data for the i'th frame.
    

    def animate(i):
        # Set the data for updated version of the line.
        #locs['u0'] = A.dot(u0)
        #print u0
        line.set_data(domain, u[i])
        # Notice that this returns a tuple of length 1.
        return line,
    a = animation.FuncAnimation(f, animate, frames=181, interval=20)
    # Show the animation.
    plt.show()      

	

def prob3():
	# Numerically approximate the solution from t = 0 to t = 2 using the given discretization, 
	# and animate your results. 

    
    m = 20
 
 
    def u0(x):
        return 0.2*np.e**(-m**2*(x-0.5)**2)
    
    def ut(x):
        return 0
        
    def ddu(x):
        return (-0.4 * m**2 + 0.8 * m**4 * (0.5-x)**2)*np.e**(-m**2 * (-0.5 + x)**2)
        
    u,domain = wave_solver(1,ut,u0,ddu,440,200,2)
    
    # Initialize a matplotlib figure.
    f = plt.figure()
    # Set the x and y axes by constructing an axes object.
    plt.axes(xlim=(0,1), ylim=(-.5,.5))
    # Plot an empty line to use in the animation.
    # Notice that we are unpacking a tuple of length 1.
    line, = plt.plot([], [])
    # Define an animation function that will update the line to
    # reflect the desired data for the i'th frame.
    

    def animate(i):
        # Set the data for updated version of the line.
        #locs['u0'] = A.dot(u0)
        #print u0
        line.set_data(domain, u[i])
        # Notice that this returns a tuple of length 1.
        return line,
    a = animation.FuncAnimation(f, animate, frames=441, interval=20)
    # Show the animation.
    plt.show()
	
def prob4():
	# Numerically approximate the solution from t = 0 to t = 2 using the given discretization, 
	# and animate your results.
	

    def u0(x):
        y = np.copy(x)
        y[y<(5/11.)]= 0
        y[y>(6/11.)]=0
        y[y != 0]= (1/3.)
        return y 
    
    def ut(x):
        return 0
        
    def ddu(x):
        return 0
        
    u,domain = wave_solver(1,ut,u0,ddu,440,200,2)
    
    # Initialize a matplotlib figure.
    f = plt.figure()
    # Set the x and y axes by constructing an axes object.
    plt.axes(xlim=(0,1), ylim=(-.5,.5))
    # Plot an empty line to use in the animation.
    # Notice that we are unpacking a tuple of length 1.
    line, = plt.plot([], [])
    # Define an animation function that will update the line to
    # reflect the desired data for the i'th frame.
    

    def animate(i):
        # Set the data for updated version of the line.
        #locs['u0'] = A.dot(u0)
        #print u0
        line.set_data(domain, u[i])
        # Notice that this returns a tuple of length 1.
        return line,
    a = animation.FuncAnimation(f, animate, frames=441, interval=20)
    # Show the animation.
    plt.show()

prob4()