# Call this lab pseudospectral2.py
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as it

from scipy.fftpack import fft, ifft

def example():
    N=50
    x1 = (2.*np.pi/N)*np.arange(1,N+1)
    f = np.sin(x1)**2.*np.cos(x1) + np.exp(2.*np.sin(x1+1))
    k = np.concatenate(( np.arange(0,N/2) ,np.array([0]) ,np.arange(-N/2+1,0,1) ))
    # Approximates the derivative using the pseudospectral method
    f_hat = fft(f)
    fp_hat = ((1j*k)*f_hat)
    fp = np.real(ifft(fp_hat))
    # Calculates the derivative analytically
    x2 = np.linspace(0,2*np.pi,200)
    derivative = (2.*np.sin(x2)*np.cos(x2)**2. -np.sin(x2)**3. +2*np.cos(x2+1)*np.exp(2*np.sin(x2+1)))
    plt.plot(x2,derivative,'-k',linewidth=2.)
    plt.plot(x1,fp,'*b')
    plt.savefig('spectral2_derivative.pdf')
    plt.show()
    

def prob1(N):
    """
    N = integer
    Define u(x) = np.sin(x)**2.*np.cos(x) + np.exp(2*np.sin(x+1)) .
    Find the Fourier approximation for .5 uprimeprime - uprime at the 
    Fourier grid points for a given N.
    Plot the true solution, along with your numerical solution.
    """
    x1 = (2.*np.pi/N)*np.arange(1,N+1)
    f = np.sin(x1)**2.*np.cos(x1) + np.exp(2.*np.sin(x1+1))
    k = np.concatenate(( np.arange(0,N/2) ,np.array([0]) ,np.arange(-N/2+1,0,1) ))
    f_hat = fft(f)
    fp_hat = ((1j*k)*f_hat)
    fpp_hat = ((-k**2)*f_hat)
    
    sol_hat = 0.5*(fpp_hat)-fp_hat
    
    sol = np.real(ifft(sol_hat))
    
    x = np.linspace(0,2*np.pi,200)
    
    analytic =  -7 * np.sin(x)**2*np.cos(x) + 2 * np.cos(x)**3 + (-2*np.sin(x+1) + 4*np.cos(x+1)**2) * np.exp(2*np.sin(x+1))
    analytic *= 0.5
    analytic -=   (2.*np.sin(x)*np.cos(x)**2. -np.sin(x)**3. +2*np.cos(x+1)*np.exp(2*np.sin(x+1)))  
    
    
    plt.plot(x,analytic,'-k',linewidth=2.)
    plt.plot(x1,sol,'*b')
    plt.savefig('spectral2_derivative.pdf')
    plt.show()


def prob2():
    """ 
    Create the 3d plot of the numerical solution from t=0 to t=8. 

    """
    N=400
    x1 = (2.*np.pi/N)*np.arange(1,N+1)
    c = 0.2+np.sin(x1-1)**2
    u = np.exp(-100*(x1-1)**2)
    k = np.concatenate(( np.arange(0,N/2) ,np.array([0]) ,np.arange(-N/2+1,0,1) ))
    
    #y0 = np.exp(-100*(x1-1)**2)
    
    def f(y,t):
        return np.real(-c*ifft((1j*k)*fft(y)))
        
    sol = it.odeint(f,u,np.linspace(0,8,N))
    
    x,y =np.meshgrid(x1,np.linspace(0,8,N))
    
    final = plt.figure()
    
    thing = final.add_subplot(111,projection='3d')
    thing.plot_surface(x,y,sol)
    plt.show()
    


prob2()


