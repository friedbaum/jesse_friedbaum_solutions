# Call this lab finite_element.py
import sys
import numpy as np
import scipy as sp
import scipy.linalg as la
import scipy.sparse as sa
import scipy.sparse.linalg as sla
import matplotlib.pyplot as plt
from scipy.optimize import root
from scipy.interpolate import BarycentricInterpolator as BI


def cheb(N):
    x = np.cos((np.pi/N)*np.linspace(0,N,N+1))
    x.shape = (N+1,1)
    lin = np.linspace(0,N,N+1)
    lin.shape = (N+1,1)
    c = np.ones((N+1,1))
    c[0], c[-1] = 2., 2.
    c = c*(-1.)**lin
    X = x*np.ones(N+1) # broadcast along 2nd dimension (columns)
    dX = X - X.T
    D = (c*(1./c).T)/(dX + np.eye(N+1))
    D = D - np.diag(np.sum(D.T,axis=0))
    x.shape = (N+1,)
    # Here we return the differentiation matrix and the Chebyshev points,
    # numbered from x_0 = 1 to x_N = -1
    return D, x
    

def analytic(x):
    alpha = 2
    beta = 4
    epsilon = 0.02
    return alpha + x + (beta - alpha - 1)*(np.exp(x/epsilon)-1)/(np.exp(1./epsilon)-1)

def finite_dif(grid, epsilon = 0.02, alpha = 2, beta = 4):#, name = '',thecolor ='r'):
    N = len(grid)-1
    h = grid - np.roll(grid,1)
    
    
    lower = np.array([epsilon]*(N+1))/np.roll(h,-1)+0.5
    lower = lower[:-1]
    lower[-1] = 0
    
    upper = np.array([epsilon]*(N+1))/h-0.5
    upper = upper[1:]
    upper[0] = 0
    
    middle =np.array([-epsilon]*(N+1))/h-np.array([epsilon]*(N+1))/np.roll(h,-1)
    middle[0],middle[-1] = 1,1
    
    #print len(upper),len(middle),len(lower)
        
    A = sa.diags([upper,middle,lower],[1,0,-1])
    

    
    phi = -0.5*(h+np.roll(h,-1))
    phi[0],phi[-1] = alpha,beta
    
    K = sla.spsolve(A,phi)
    

    
    #print A.todense()
    #print phi
    #print K
    

    
    #plt.plot(np.linspace(0,1,101),analytic(np.linspace(0,1,101)),label = 'Actual Solution')
    #plt.plot(grid,K, 'ro', ls = '-', label = name, color = thecolor)

    #plt.legend()
    #plt.plot()
    return K


def prob1(epsilon = 0.02, alpha = 2, beta = 4, N = 15):
    """"
    Plot the true solution, along with your numerical solution.
    """
    def analytic(x):
        return alpha + x + (beta - alpha - 1)*(np.exp(x/epsilon)-1)/(np.exp(1./epsilon)-1)
        
    h = 1./N
    
    domain = np.linspace(0,1,N+1)
    
    lower = np.array([epsilon/h+0.5]*N)
    lower[-1] = 0
    
    upper = np.array([epsilon/h-0.5]*N)
    upper[0] = 0
    
    middle = np.array([-epsilon/h-epsilon/h]*(N+1))
    middle[0],middle[-1] = 1,1
        
    A = sa.diags([upper,middle,lower],[1,0,-1])
    

    
    phi = np.array([-h]*(N+1))
    phi[0],phi[-1] = alpha,beta
    
    K = sla.spsolve(A,phi)
    

    
    #print A.todense()
    #print phi
    #print K
    

    
    plt.plot(np.linspace(0,1,101),analytic(np.linspace(0,1,101)),label = 'Actual Solution')
    plt.plot(domain,K, 'ro', ls = '-', label = 'Numeric Solution')

    plt.legend()
    plt.plot()
    
    
    
    
	


def prob2():
    """ 
    Plot the true solution, and your numerical solutions. Reproduce the 
    graph shown in the lab.  
    """
    plt.plot(np.linspace(0,1,101),analytic(np.linspace(0,1,101)),label = 'Actual Solution')
    plt.plot(np.linspace(0,1,15)**(1./8),finite_dif(np.linspace(0,1,15)**(1./8)), 'o', ls = '-',label = 'Clustered')
    plt.plot(np.linspace(0,1,15),finite_dif(np.linspace(0,1,15)), 'o', ls = '-',label = 'Evenly Spaced')
    plt.legend()
    plt.show()


def prob3():
    """ 
    Plot and show a log-log graph, showing both the error in the 
    second order finite element approximations, and the error in the 
    pseudospectral approximations. 

    You can verify the error for the second order finite element 	
    approximations using the graph shown in the lab. 
    """
    
    #func = lambda x:  -1/0.02   
    
    error = []
    perror = []    
    
    for i in xrange(4,22):
        grid = np.linspace(0,1,2**i)
        aprox = finite_dif(grid)
        actual = analytic(grid)
        error.append(la.norm(actual-aprox)/(2**i))
        
    for i in xrange(4,10):
        D,x = cheb(2**i)
        D2 = D.dot(D)
        '''
        A = D2 - D/0.02
        F = np.array([-1/0.02]*len(x))#func(x)
        A[0] = np.zeros(2**i+1)
        A[-1] = np.zeros(2**i+1)
        A[0,0] = 1
        A[-1,-1] = 1
        F[0] = 4
        F[-1] = 2
        #print A
        u = la.solve(A,F)
        '''
        def F(u):
            output = D2.dot(u) - D.dot(u)/0.02 + 1/0.02
            output[0] = u[0]-4
            output[-1] = u[-1]-1
            return output
        guess = np.ones(2**i+1)
        solution = root(F,guess).x
        solution = BI(x,solution)
        actual = analytic((x+1)*0.5)
        perror.append(la.norm(actual-solution((x+1)*0.5))/(2**i))
        if 0:#i == 10:
            plt.plot((x+1)*0.5,actual,label='actual')
            plt.plot((x+1)*0.5,solution((x+1)*0.5),label='aprox')
            plt.legend()
            plt.show()
        
    plt.loglog([2**y for y in range(4,22)],error,label='finite element')
    plt.loglog([2**y for y in range(4,10)],perror,label='psuedospectral')
    plt.legend(loc = 0)
    plt.show()
    print "I notice that psuedospectral get's considerably closer before roundoff error inhibits it's accuracy."
        
        
  
