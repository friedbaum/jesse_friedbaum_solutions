from __future__ import division
import numpy as np
import scipy as sp
import scipy.optimize as opt
from matplotlib import pyplot as plt

g = 9.81

M, m = 23., 5.
l=4.
q1, q2, q3, q4 = 1., 1., 1., 1.
r=5.

def linearized_init(M, m, l, q1, q2, q3, q4, r):
    '''
    Parameters:
        ----------
        M, m: floats
            masses of the rickshaw and the present
        l   : float
            length of the rod
        q1, q2, q3, q4, r : floats
            relative weights of the position and velocity of the rickshaw, the angular displacement theta and the change in theta, and the control
    Return
    -------
    A : ndarray
    B : ndarray
    Q : ndarray
    R : ndarray
    '''
    A = np.zeros((4,4))
    A[0,1] = 1
    A[1,2] = m*g/M
    A[2,3] = 1
    A[3,2] = g*(M+m)/(M*l)
    
    B = np.zeros((4,1))
    B[1] = M**-1
    B[3] = (M*l)**-1
    
    Q = np.diag(np.array([q1,q2,q3,q4]))
    
    R = np.array([r])
    
    return A,B,Q,R
    
def find_P(A,B,Q,R):
    '''
    Parameters:
    ----------
    A, B, Q : ndarrays of shape (4,4)
    R       : ndarray of shape (1,1)
    Returns
    -------
    P       : the matrix solution of the Riccati equation
    '''
    def func(P):
        P= P.reshape((4,4))
        return (np.dot(P,A)+A.T.dot(P) + Q -np.dot(P,B*R**-1).dot(B.T.dot(P))).reshape(16)
    '''
    guess = np.eye(4)
    guess = guess.reshape(16)
    '''
    guess = np.ones(16)*100
    
    output = opt.root(func,guess)
    #print output['x']
    
    return output['x'].reshape((4,4))
    
M, m = 23., 5.
l = 4.
q1, q2, q3, q4 = 1., 1., 1., 1.
r = 10.
tf = 15
X0 = np.array([-1, -1, .1, -.2])
    
def rickshaw(tv,X0,A, B, Q, R_inv, P):
    '''
    Parameters:
    ----------
    tv : ndarray of time values, with shape (n+1,)
    X0 :
    A, B, Q : ndarrays of shape (4,4)
    R_inv : ndarray of shape (1,1), inverse of R
    P : ndarray of shape (4,4)
    
    Returns
    -------
    Z : ndarray of shape (n+1,4), the state vector at each time
    U : ndarray of shape (n,), the control values
    '''

    def func(y,t):
        return (A - (B*R_inv).dot(B.T.dot(P))).dot(y)
        
    
        
    solution = sp.integrate.odeint(func,X0,tv)
    
    return solution
    
def prob4():
    
    M, m = 23., 5.
    l = 4.
    q1, q2, q3, q4 = 1., 1., 1., 1.
    r = 10.
    tf = 60
    X0 = np.array([-1, -1, .1, -.2])
    
    tv =  np.linspace(0,tf,500)
    
    A,B,Q,R = linearized_init(M, m, l, q1, q2, q3, q4, r)
    
    #print A
    #print B
    #print Q
    #print R
    #print sp.linalg.solve_continuous_are(A,B,Q,R.reshape((1,1)))
    
    builtin = rickshaw(tv,X0,A,B,Q,R**-1,sp.linalg.solve_continuous_are(A,B,Q,R.reshape((1,1))))
    
    plt.figure(0)
    plt.title('Built in Solver')
    plt.plot(tv,builtin[:,0],label='x')
    plt.plot(tv,builtin[:,1],label='x prime')
    plt.plot(tv,builtin[:,2],label='theta')
    plt.plot(tv,builtin[:,3],label='theta prime')
    
    plt.legend(loc = 0)
    plt.show()
    
    plt.figure(2)    
    
    plt.title('Custom Solver')    
    
    selfmade = rickshaw(tv,X0,A,B,Q,R**-1,find_P(A,B,Q,R))
    
    
    plt.plot(tv,selfmade[:,0],label='x')
    plt.plot(tv,selfmade[:,1],label='x prime')
    plt.plot(tv,selfmade[:,2],label='theta')
    plt.plot(tv,selfmade[:,3],label='theta prime')
    
    
    plt.legend(loc = 0)
    plt.show()
    
    
def prob5():
    M, m = 23., 5.
    l=4.
    q1, q2, q3, q4 = 1., 1., 1., 1.
    r=10.
    tf = 60
    X0 = np.array([-1, -1, .5, -.8])
    
    tv =  np.linspace(0,tf,500)
    
    A,B,Q,R = linearized_init(M, m, l, q1, q2, q3, q4, r)
    
    #print A
    #print B
    #print Q
    #print R
    #print sp.linalg.solve_continuous_are(A,B,Q,R.reshape((1,1)))
    
    builtin = rickshaw(tv,X0,A,B,Q,R**-1,sp.linalg.solve_continuous_are(A,B,Q,R.reshape((1,1))))
    
    plt.figure(0)
    plt.title('Built in Solver')
    plt.plot(tv,builtin[:,0],label='x')
    plt.plot(tv,builtin[:,1],label='x prime')
    plt.plot(tv,builtin[:,2],label='theta')
    plt.plot(tv,builtin[:,3],label='theta prime')
    
    plt.legend(loc = 0)
    plt.show()
    print 'The control is stable up to very large values, but the max value of x prime must be very high to stabalize these systems.  In actuality the theta prime should be kept less than one and theta should probably be kept below three of four.'


if __name__=='__main__':
    prob4()