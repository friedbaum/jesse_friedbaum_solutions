import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import newton

def EmbeddingAlg(param_list, guess, F):
    X = []
    for param in param_list:
        try:
            x_value = newton(F, guess, fprime=None, args=(param,), tol = 1E-7, maxiter = 50)
            X.append(x_value)
            guess = x_value
        except RuntimeError:
            return param_list[:len(X)],X
    return param_list, X
    
def F(x, lmbda):
    return x**2 + lmbda
'''    
C1, X1 = EmbeddingAlg(np.linspace(-5, 0, 200), np.sqrt(5), F)
C2, X2 = EmbeddingAlg(np.linspace(-5, 0, 200), -np.sqrt(5), F)
plt.plot(C1,X1)
plt.plot(C2,X2)
'''

def prob1():
    # Using the natural embedding algorithm, produce the plot 
    # requested in exercise 1 in the lab.
    def func(x,lmbda):
        return lmbda * x - x**3
    
    for i in xrange(-1,2,1):
        #print i
        C1, X1 = EmbeddingAlg(np.linspace(1, -1, 200), i, func)

        plt.plot(C1,X1)
    plt.show()
	
	

def prob2():
    # Using the natural embedding algorithm, produce the plots 
    # requested in exercise 2 in the lab.
    def func(x,(args)):
        return args[0] + args[1] * x - x**3

    f,plots = plt.subplots(4, sharex = True, sharey = True)        
    
    guesses = [(-2,0,2),(-1,0,1),(-1,0,1),(-2,0,2)]    
    
    etas = [-1,-.2,.2,1]
    for i in xrange(4):
        parameters = np.empty((200,2))
        parameters[:,0] = etas[i]
        parameters[:,1] = np.linspace(2.5,0,200)
        for j in guesses[i]:
            print j
            C1, X1 = EmbeddingAlg(parameters, j, func)
            #print C1
            #print X1

            plots[i].plot(C1[:,1],X1)
        plots[i].set_title('eta = ' + str(etas[i]))
    
        
    
    plt.show()
	
	

def prob3():
    # Using the natural embedding algorithm, produce the plot 
    # requested in exercise 3 in the lab.
    r = 0.56
    def func(x,k):
        return r*x*(1-x/k)-x**2/(1+x**2)
    parameters = [np.linspace(1,12,300), np.linspace(6.5,10,300),np.linspace(15,6,300)]
    guesses = [1,2,10]
    for i in xrange(3):
        C1, X1 = EmbeddingAlg(parameters[i], guesses[i], func)
        plt.plot(C1,X1)
    plt.show()

prob3()