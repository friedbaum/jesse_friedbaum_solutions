import numpy as np
from matplotlib import pyplot as plt
from math import floor

def mm(a,b):
    output = np.copy(a)
    output[a*b < 0] = 0
    mask = np.abs(b) < np.abs(a)
    output[mask] = b[mask]
    
    return output
    
        

def upwind(u0, a, xmin, xmax, t_final, nt, N = 'unspecified'):
    """ Solve the advection equation with periodic
    boundary conditions on the interval [xmin, xmax]
    using the upwind finite volume scheme.
    Use u0 as the initial conditions.
    a is the constant from the PDE.
    Use the size of u0 as the number of nodes in
    the spatial dimension.
    Let nt be the number of spaces in the time dimension
    (this is the same as the number of steps if you do
    not include the initial state).
    Plot and show the computed solution along
    with the exact solution. """
    dt = float(t_final) / nt
    # Since we are doing periodic boundary conditions,
    # we need to divide by u0.size instead of (u0.size - 1).
    dx = float(xmax - xmin) / u0.size
    lambda_ = a * dt / dx
    u = u0.copy()
    for j in xrange(nt):
        # The Upwind method. The np.roll function helps us
        # account for the periodic boundary conditions.
        u -= lambda_ * (u - np.roll(u, 1))
    # Get the x values for the plots.
    x = np.linspace(xmin, xmax, u0.size+1)[:-1]
    # Plot the computed solution.
    plt.plot(x, u, label='Upwind Method N = '+str(N))
    # Find the exact solution and plot it.
    #distance = a * t_final
    #roll = int((distance - floor(distance)) * u0.size)
    #plt.plot(x, np.roll(u0, roll), label='Exact solution')
    # Show the plot with the legend.
    plt.legend(loc='best')
    #plt.show()


def lax_wendroff(u0, a, xmin, xmax, t_final, nt, N = 'unspecified'):
    """ Solve the advection equation with periodic
    boundary conditions on the interval [xmin, xmax]
    using the Lax-Wendroff finite volume scheme.
    Use u0 as the initial conditions.
    a is the constant from the PDE.
    Use the size of u0 as the number of nodes in
    the spatial dimension.
    Let nt be the number of spaces in the time dimension
    (this is the same as the number of steps if you do
    not include the initial state).
    Plot and show the computed solution along
    with the exact solution. """
    dt = float(t_final) / nt
    # Since we are doing periodic boundary conditions,
    # we need to divide by u0.size instead of (u0.size - 1).
    dx = float(xmax - xmin) / u0.size
    lambda_ = dt / dx
    u = u0.copy()
    for j in xrange(nt):
        # The Upwind method. The np.roll function helps us
        # account for the periodic boundary conditions.
        F_ = a *(np.roll(u,1)+(u-np.roll(u,1))*(dx-a*dt)/(2.*dx))
        F = a *(u+(np.roll(u,-1)-u)*(dx-a*dt)/(2.*dx))
        u -= lambda_ * (F-F_)
    # Get the x values for the plots.
    x = np.linspace(xmin, xmax, u0.size+1)[:-1]
    # Plot the computed solution.
    plt.plot(x, u, label='Wax Wendroff N = '+ str(N))
    # Find the exact solution and plot it.
    #distance = a * t_final
    #roll = int((distance - floor(distance)) * u0.size)
    #plt.plot(x, np.roll(u0, roll), label='Exact solution')
    # Show the plot with the legend.
    plt.legend(loc='best')
    #plt.show()

def minimod(u0, a, xmin, xmax, t_final, nt, N = 'unspecified'):
    """ Solve the advection equation with periodic
    boundary conditions on the interval [xmin, xmax]
    using the minimod finite volume scheme.
    Use u0 as the initial conditions.
    a is the constant from the PDE.
    Use the size of u0 as the number of nodes in
    the spatial dimension.
    Let nt be the number of spaces in the time dimension
    (this is the same as the number of steps if you do
    not include the initial state).
    Plot and show the computed solution along
    with the exact solution. """
    dt = float(t_final) / nt
    # Since we are doing periodic boundary conditions,
    # we need to divide by u0.size instead of (u0.size - 1).
    dx = float(xmax - xmin) / u0.size
    lambda_ = dt / dx
    u = u0.copy()
    for j in xrange(nt):
        # The Upwind method. The np.roll function helps us
        # account for the periodic boundary conditions.
        m_ = mm((u-np.roll(u,1))/dx,(np.roll(u,1)-np.roll(u,2))/dx)
        F_ = a *(np.roll(u,1)+m_*(dx-a*dt)/2.)
        m = mm((np.roll(u,-1)-u)/dx,(u-np.roll(u,1))/dx)
        F = a *(u+m*(dx-a*dt)/2.)
        u -= lambda_ * (F-F_)
    # Get the x values for the plots.
    x = np.linspace(xmin, xmax, u0.size+1)[:-1]
    # Plot the computed solution.
    plt.plot(x, u, label='Min Mod N = '+ str(N))
    # Find the exact solution and plot it.
    #distance = a * t_final
    #roll = int((distance - floor(distance)) * u0.size)
    #plt.plot(x, np.roll(u0, roll), label='Exact solution')
    # Show the plot with the legend.
    plt.legend(loc='best')
    #plt.show()


# Define the initial conditions.
# Leave off the last point since we're using periodic
# boundary conditions.
'''
nx = 100
a = 1.2
t_final=1.2
nt = nx * 3 // 2
x = np.linspace(0., 1., nx+1)[:-1]
u0 = np.exp(-(x - .3)**2 / .005)
arr = (.6 < x) & (x < .7 )
u0[arr] += 1.
distance = a * t_final
roll = int((distance - floor(distance)) * u0.size)
plt.plot(x, np.roll(u0, roll), label='Exact solution')  
# Run the simulation.
#upwind(u0, 1.2, 0, 1, 1.2, nt)
lax_wendroff(u0, 1.2, 0, 1, 1.2, nt,nx)
minimod(u0, 1.2, 0, 1, 1.2, nt,nx)

plt.show()
'''


def prob1():
    
    a = 1.2
    t_final = 1.2    
       
    for i in [30,60,120,240]:
        nx = i
        nt = nx * 3 // 2
        x = np.linspace(0., 1., nx+1)[:-1]
        u0 = np.exp(-(x - .3)**2 / .005)
        arr = (.6 < x) & (x < .7 )
        u0[arr] += 1.
        distance = a * t_final
        roll = int((distance - floor(distance)) * u0.size)
        plt.plot(x, np.roll(u0, roll), label='Exact solution')  
        # Run the simulation.
        upwind(u0, 1.2, 0, 1, 1.2, nt,i)
        lax_wendroff(u0, 1.2, 0, 1, 1.2, nt,i)

        plt.show()
        
def prob2():
    
    a = 1.2
    t_final = 1.2    
       
    for i in [30,60,120,240]:
        nx = i
        nt = nx * 3 // 2
        x = np.linspace(0., 1., nx+1)[:-1]
        u0 = np.exp(-(x - .3)**2 / .005)
        arr = (.6 < x) & (x < .7 )
        u0[arr] += 1.
        distance = a * t_final
        roll = int((distance - floor(distance)) * u0.size)
        plt.plot(x, np.roll(u0, roll), label='Exact solution')  
        # Run the simulation.
        minimod(u0, 1.2, 0, 1, 1.2, nt,i)
        lax_wendroff(u0, 1.2, 0, 1, 1.2, nt,i)

        plt.show()
    
prob2()