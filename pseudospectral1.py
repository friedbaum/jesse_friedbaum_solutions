# Call this lab pseudospectral1.py

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import BarycentricInterpolator as BI
import scipy.linalg as la
from scipy.optimize import root
from scipy.optimize import fsolve
from mpl_toolkits.mplot3d import Axes3D

def cheb(N):
    x = np.cos((np.pi/N)*np.linspace(0,N,N+1))
    x.shape = (N+1,1)
    lin = np.linspace(0,N,N+1)
    lin.shape = (N+1,1)
    c = np.ones((N+1,1))
    c[0], c[-1] = 2., 2.
    c = c*(-1.)**lin
    X = x*np.ones(N+1) # broadcast along 2nd dimension (columns)
    dX = X - X.T
    D = (c*(1./c).T)/(dX + np.eye(N+1))
    D = D - np.diag(np.sum(D.T,axis=0))
    x.shape = (N+1,)
    # Here we return the differentiation matrix and the Chebyshev points,
    # numbered from x_0 = 1 to x_N = -1
    return D, x


def prob1():
    domain = np.linspace(-1,1,100)
    def func(x):
        return np.cos(6*x)*np.e**x
    def der(x):
        return np.cos(6*x)*np.e**x - 6*np.sin(6*x)*np.e**x
    tests = [6,8,10]
    for n in tests:
        D,x = cheb(n)
        points = func(x)
        aprox_der = D.dot(points)
        aprox_der = BI(x,aprox_der)
        plt.plot(domain,aprox_der(domain),label= 'n = '+str(n))
    plt.plot(domain,der(domain),label = 'actual')
    plt.legend()
    plt.show()
        
        
     
	


def prob2(N=20):
    """ 
    Plot the true solution, and your numerical solution. Choose a suitable number
    of Chebychev points for the pseudospectral method.
    	
    """
    def exact(x):
        return (np.e**(2.*x)-np.cosh(2.)-np.sinh(2.)*x)/4.
        
    def func(x):
        return np.e**(2*x)
        
    domain = np.linspace(-1,1,100)
    D,x = cheb(N)
    D2 = D.dot(D)
    F = func(x)
    D2[0] = np.zeros(N+1)
    D2[-1] = np.zeros(N+1)
    D2[0,0] = 1
    D2[-1,-1] = 1
    F[0] = 0
    F[-1] = 0
    u = la.solve(D2,F)
    plt.plot(x,u,label = 'approximate')
    
    plt.plot(domain,exact(domain),label='exact')
    
    plt.legend()
    plt.show()
    


def prob3(N=20):
    """ 
    Plot and show your numerical solution. Choose a suitable number
    of Chebychev points for the pseudospectral method.
    	
    """
    def func(x):
        return np.e**(3*x)
        
    D,x = cheb(N)
    D2 = D.dot(D)
    A = D2 + D
    F = func(x)
    A[0] = np.zeros(N+1)
    A[-1] = np.zeros(N+1)
    A[0,0] = 1
    A[-1,-1] = 1
    F[0] = -1
    F[-1] = 2
    u = la.solve(A,F)
    plt.plot(x,u,label = 'approximate')
    
    plt.show()



def prob4(N = 100):
    """ 
    Plot and show your numerical solution. Choose a suitable number
    of Chebychev points for the pseudospectral method.
    	
    """
    Domain = np.linspace(-1,1,400)
    D,x = cheb(N)
    D2 = D.dot(D)
    
        
    
    lambdas = [4,8,12]
    for l in lambdas:
        def F(u):
            output = D2.dot(u) - l*np.sinh(l*u)
            output[0] = u[0]-1
            output[-1] = u[-1]
            return output
        guess = np.ones(N+1)
        solution = root(F,guess).x
        solution = BI(x,solution)
        plt.plot(0.5*(Domain+1),solution(Domain),label = 'Lambda = ' + str(l))
    plt.legend()
    plt.show()
        
        


def prob5(N=100):
    """ 
    Plot and show your numerical solution. On a separate plot, graph the minimizing surface. 
    return chebychev_grid, numerical_solution
    	
    """
    #Domain = np.linspace(-1,1,400)
    D,x = cheb(N)
    D2 = D.dot(D)
    D[0] = np.zeros(N+1)
    D[-1] = np.zeros(N+1)
    D[0,0] = 1
    D[-1,-1] = 1
    D2[0] = np.zeros(N+1)
    D2[-1] = np.zeros(N+1)
    D2[0,0] = 1
    D2[-1,-1] = 1
    
    #print D
    #print D2

    def F(y):
        output = y * (D2.dot(y)) - (D.dot(y))**2 - np.ones(N+1)
        output[0] = y[0] -7
        output[-1] = y[-1] -1
        return output
        
    f = np.ones(N+1)*7.
    f[0] = 7
    f[-1] = 1
    #print f
    solution = fsolve(F,f)
    #print solution
    
    plt.plot(x,solution)
    plt.show()
    
    
    phi = np.linspace(0, 2*np.pi, N+1)    
    Radius, Phi = np.meshgrid(solution, phi)
    X, Y, Z = Radius * np.cos(Phi), Radius * np.sin(Phi), x
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(Y, X, Z)
    ax.plot_surface(Y, -1. * X, Z)
    plt.show()
    
    return x, solution
    
    
    
    
    