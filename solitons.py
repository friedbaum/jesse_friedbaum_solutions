from __future__ import division
from math import sqrt, pi
import numpy as np					
from scipy.fftpack import fft, ifft		
from mpl_toolkits.mplot3d.axes3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm


def initialize_all(y0, t0, t1, n):
    """ An initialization routine for the different ODE solving
    methods in the lab. This initializes Y, T, and h. """
    if isinstance(y0, np.ndarray):
        Y = np.empty((n, y0.size),dtype=complex).squeeze()
    else:
        Y = np.empty(n,dtype=complex)
    Y[0] = y0
    T = np.linspace(t0, t1, n)
    h = float(t1 - t0) / (n - 1)
    return Y, T, h



def RK4(f, y0, t0, t1, n):
    Y, T, h = initialize_all(y0, t0, t1, n)
    for i in xrange (1, n):
        K1 = f(T[i-1], Y[i-1])
        tplus = (T[i] + T[i-1]) * .5
        K2 = f(tplus, Y[i-1] + .5 * h * K1)
        K3 = f(tplus, Y[i-1] + .5 * h * K2)
        K4 = f(T[i], Y[i-1] + h * K3)
        Y[i] = Y[i-1] + (h / 6.) * (K1 + 2 * K2 + 2 * K3 + K4)
    return T, Y






def prob1():
    # Plot and show the numerical solution

    N = 256

    k=np.concatenate((np.arange(0,N/2),np.array([0]),np.arange(-N/2+1,0,1) )).reshape(N,)
    
    ik3 = 1j*k**3.
    def G_unscaled(t,y):
        out = -.5*1j*k*fft(ifft(y,axis=0)**2.,axis=0) + ik3*y
        return out
        

    x = (2.*np.pi/N)*np.arange(-N/2,N/2).reshape(N,1)
    # Space discretization
    s, shift = 25.**2., 2.
    # Initial data is a soliton
    y0 = (3.*s*np.cosh(.5*(sqrt(s)*(x+shift)))**(-2.)).reshape(N,)
    # Solves the ODE.
    max_t = 0.0075
    dt =  0.09*N**(-2.)
    max_tsteps = int(round(max_t/dt))
    y0 = fft(y0,axis=0)
    T,Y = RK4(G_unscaled, y0, t0=0, t1=max_t, n=max_tsteps)
    # Using the variable stride, we step through the data,
    # applying the inverse fourier transform to obtain u.
    # These values will be plotted.
    stride = int(np.floor((max_t/25.)/dt))
    uvalues, tvalues = np.real(ifft(y0,axis=0)).reshape(N,1), np.array(0.).reshape(1,1)
    for n in range(1,max_tsteps+1):
        if np.mod(n,stride) == 0:
            t = n*dt
            u = np.real( ifft(Y[n], axis=0) ).reshape(N,1)
            uvalues = np.concatenate((uvalues,np.nan_to_num(u)),axis=1)
            tvalues = np.concatenate((tvalues,np.array(t).reshape(1,1)),axis=1)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=45., azim=150)
    tv, xv = np.meshgrid(tvalues,x,indexing='ij')
    surf = ax.plot_surface(tv,xv, uvalues.T, rstride=1, cstride=1, cmap=cm.coolwarm,linewidth=0, antialiased=False)
    tvalues = tvalues[0]; ax.set_xlim(tvalues[0], tvalues[-1])
    ax.set_ylim(-pi, pi); ax.invert_yaxis()
    ax.set_zlim(0., 4000.)
    ax.set_xlabel('T'); ax.set_ylabel('X'); ax.set_zlabel('Z')
    plt.show()

    return dt



def prob2():
    # Plot and show the numerical solution

    N = 256

    k=np.concatenate((np.arange(0,N/2),np.array([0]),np.arange(-N/2+1,0,1) )).reshape(N,)
    
    ik3 = 1j*k**3.
    def G_unscaled(t,y):
        out = -.5*1j*k*fft(ifft(y,axis=0)**2.,axis=0) + ik3*y
        return out
        

    x = (2.*np.pi/N)*np.arange(-N/2,N/2).reshape(N,1)
    # Space discretization
    s1, shift1 = 25.**2., 2.
    s2, shift2 = 16.**2., 1.
    # Initial data is a soliton
    y0 = (3.*s1*np.cosh(.5*(sqrt(s1)*(x+shift1)))**(-2.)+3.*s2*np.cosh(.5*(sqrt(s2)*(x+shift2)))**(-2.)).reshape(N,)
    # Solves the ODE.
    max_t = 0.0075
    dt =  0.09*N**(-2.)
    max_tsteps = int(round(max_t/dt))
    y0 = fft(y0,axis=0)
    T,Y = RK4(G_unscaled, y0, t0=0, t1=max_t, n=max_tsteps)
    # Using the variable stride, we step through the data,
    # applying the inverse fourier transform to obtain u.
    # These values will be plotted.
    stride = int(np.floor((max_t/25.)/dt))
    uvalues, tvalues = np.real(ifft(y0,axis=0)).reshape(N,1), np.array(0.).reshape(1,1)
    for n in range(1,max_tsteps+1):
        if np.mod(n,stride) == 0:
            t = n*dt
            u = np.real( ifft(Y[n], axis=0) ).reshape(N,1)
            uvalues = np.concatenate((uvalues,np.nan_to_num(u)),axis=1)
            tvalues = np.concatenate((tvalues,np.array(t).reshape(1,1)),axis=1)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=45., azim=150)
    tv, xv = np.meshgrid(tvalues,x,indexing='ij')
    surf = ax.plot_surface(tv,xv, uvalues.T, rstride=1, cstride=1, cmap=cm.coolwarm,linewidth=0, antialiased=False)
    tvalues = tvalues[0]; ax.set_xlim(tvalues[0], tvalues[-1])
    ax.set_ylim(-pi, pi); ax.invert_yaxis()
    ax.set_zlim(0., 4000.)
    ax.set_xlabel('T'); ax.set_ylabel('X'); ax.set_zlabel('Z')
    plt.show()

    return dt


def prob3(N = 256,dt = 0.431*256**(-2.)):
    # Plot and show the numerical solution
    # N is the number of subintervals in x
    #return (solution at the final time)

    #N = 256

    k=np.concatenate((np.arange(0,N/2),np.array([0]),np.arange(-N/2+1,0,1) )).reshape(N,)
    
    ik3 = 1j*k**3.
    def G_unscaled(t,y):
        out = -.5*1j*k*fft(ifft(y/np.exp(-1.*ik3*t),axis=0)**2.,axis=0)*np.exp(-1.*ik3*t)
        return out
        

    x = (2.*np.pi/N)*np.arange(-N/2,N/2).reshape(N,1)
    # Space discretization
    s1, shift1 = 25.**2., 2.
    s2, shift2 = 16.**2., 1.
    # Initial data is a soliton
    y0 = (3.*s1*np.cosh(.5*(sqrt(s1)*(x+shift1)))**(-2.)+3.*s2*np.cosh(.5*(sqrt(s2)*(x+shift2)))**(-2.)).reshape(N,)
    # Solves the ODE.
    max_t = 0.0075
    dt =  0.431*N**(-2.)
    max_tsteps = int(round(max_t/dt))
    y0 = fft(y0,axis=0)
    T,Y = RK4(G_unscaled, y0, t0=0, t1=max_t, n=max_tsteps)
    #print T.shape
    #print len(Y[-1,:])
    #print 0.09*256**(-2.)
    Y /= np.exp(-1.*ik3*np.vstack(T))
    # Using the variable stride, we step through the data,
    # applying the inverse fourier transform to obtain u.
    # These values will be plotted.
    stride = int(np.floor((max_t/25.)/dt))
    uvalues, tvalues = np.real(ifft(y0,axis=0)).reshape(N,1), np.array(0.).reshape(1,1)
    for n in range(1,max_tsteps+1):
        if np.mod(n,stride) == 0:
            t = n*dt
            u = np.real( ifft(Y[n], axis=0) ).reshape(N,1)
            uvalues = np.concatenate((uvalues,np.nan_to_num(u)),axis=1)
            tvalues = np.concatenate((tvalues,np.array(t).reshape(1,1)),axis=1)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=45., azim=150)
    tv, xv = np.meshgrid(tvalues,x,indexing='ij')
    surf = ax.plot_surface(tv,xv, uvalues.T, rstride=1, cstride=1, cmap=cm.coolwarm,linewidth=0, antialiased=False)
    tvalues = tvalues[0]; ax.set_xlim(tvalues[0], tvalues[-1])
    ax.set_ylim(-pi, pi); ax.invert_yaxis()
    ax.set_zlim(0., 4000.)
    ax.set_xlabel('T'); ax.set_ylabel('X'); ax.set_zlabel('Z')
    plt.show()

    return dt
	
	
print prob3()
