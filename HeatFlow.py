# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 09:16:27 2015

@author: fried@byu.local
"""

# Call your solutions file heatflow.py


def prob1():
    # For the initial boundary value problem given, plot
    # the initial data and the approximation at t = .4 on the same figure.
    
    pass
    	
	
	
def prob2():
    # Produce two animations using the given discretizations in space and time. 
    # The second animation should show instability in the numerical solution, due to 
    # the CFL condition being broken. 
    	
    # You may use either matplotlib or mayavi for the animations.
    pass
	

def prob3():
    # Reproduce the loglog plot in the lab to demonstrate 2nd order convergence of the 
    # Crank-Nicolson method. 
    pass